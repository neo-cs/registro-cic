/*
 * Copyright 2011 Freddy Barrera.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.event.InputEvent;
import java.awt.AWTException;
import java.awt.Robot;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Freddy Barrera
 */
public class VentanaAccesoTest {

    public VentanaAccesoTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test of main method, of class VentanaAcceso.
     */
    @Test
    public void testMain() {
        try {
            //http://darkbyteblog.wordpress.com/2011/01/17/java-clase-java-awt-robot-control-del-raton-teclado-y-captura-de-pantalla/
            System.out.println("main");
            String[] args = null;
            VentanaAcceso.main(args);
            Robot robot = new Robot();

            //cambia la posición en pantalla del puntero a las coordenadas
            //X=300 e Y=600.
            robot.mouseMove(300, 600);

            //click con el botón derecho
            robot.mousePress(InputEvent.BUTTON3_MASK);
            robot.mouseRelease(InputEvent.BUTTON3_MASK);

        } catch (AWTException ex) {
            Logger.getLogger(VentanaAccesoTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
