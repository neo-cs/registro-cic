/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mx.uacam.balam.simulacion.registrocic.data.dao;

import java.util.Calendar;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPcId;
import org.hibernate.query.Query;

/**
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class PrestamoPcDAOHibernate extends GenericHibernateDAO<PrestamoPc, PrestamoPcId> implements PrestamoPcDAO {

    private static final String QUERY_PRESTAMO =
            "update PrestamoPc as pres"
            + "   set pres.horaFin = :horaFin,"
            + "       pres.computadora.estadoPc.clave = 1,"
            + " where pres.alumno.matricula = :matricula"
            + "   and pres.horaFin is null";

    private static final String QUERY_TIENE_PRESTAMO =
            "  select case count(p.id.matricula)"
            + "       when 0 then false"
            + "       else true"
            + "       end"
            + "  from PrestamoPc as p"
            + " where p.id.matricula = :matricula"
            + "   and p.horaFin is null";

    @Override
    public boolean liberar(Integer matricula) {
        Query<PrestamoPc> consulta = getSession()
                .createQuery(QUERY_PRESTAMO, PrestamoPc.class);
        consulta.setParameter("matricula", matricula);
        consulta.setParameter("horaFin", Calendar.getInstance().getTime());
        int updateRows = consulta.executeUpdate();
        return updateRows > 0;
    }

    @Override
    public boolean tienePrestamo(Integer matricula) {
        Query<Boolean> consulta = getSession()
                .createQuery(QUERY_TIENE_PRESTAMO, Boolean.class);
        consulta.setParameter("matricula", matricula);
        return consulta.getSingleResult();
    }

}
