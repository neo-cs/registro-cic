/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import mx.uacam.balam.simulacion.registrocic.data.entities.Alumno;
import mx.uacam.balam.simulacion.registrocic.data.entities.Computadora;
import mx.uacam.balam.simulacion.registrocic.data.entities.Especialidad;
import mx.uacam.balam.simulacion.registrocic.data.entities.EstadoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.Localizacion;
import mx.uacam.balam.simulacion.registrocic.data.entities.Maestro;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoSala;
import mx.uacam.balam.simulacion.registrocic.data.entities.Sala;
import mx.uacam.balam.simulacion.registrocic.data.entities.Software;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory object.
 *
 * @author Freddy Barrera
 */
public class HibernateUtil {

    public static final SessionFactory SESSION_FACTORY;
    private static final Logger LOGGER = LogManager.getLogger(HibernateUtil.class);

    static {
        try (InputStream is = HibernateUtil.class.getClassLoader()
                .getResourceAsStream("mx/uacam/balam/simulacion/registrocic/data"
                        + "/configuration.properties")) {
            Properties properties = new Properties();
            properties.load(is);

            String userHome = System.getProperty("user.home", ".");
            String separator =  System.getProperty("file.separator");
            String url = MessageFormat.format("jdbc:hsqldb:file:{0}{4}{1}{4}{2}{4}{3}",
                    userHome,                              // 0
                    properties.getProperty("cic.app.dir"), // 1
                    properties.getProperty("cic.db.dir"),  // 2
                    properties.getProperty("cic.db.name"), // 3
                    separator);                            // 4
            Configuration cfg = new Configuration();

            cfg.setProperty("hibernate.dialect", properties.getProperty("cic.hibernate.dialect"));
            cfg.setProperty("hibernate.connection.driver_class", properties.getProperty("cic.db.driver"));
            cfg.setProperty("hibernate.connection.url", url);
            cfg.setProperty("hibernate.connection.username", properties.getProperty("cic.db.username"));
            cfg.setProperty("hibernate.connection.password", properties.getProperty("cic.db.password"));

            cfg.addClass(Alumno.class);
            cfg.addClass(Computadora.class);
            cfg.addClass(Especialidad.class);
            cfg.addClass(EstadoPc.class);
            cfg.addClass(Localizacion.class);
            cfg.addClass(Maestro.class);
            cfg.addClass(PrestamoPc.class);
            cfg.addClass(PrestamoSala.class);
            cfg.addClass(Sala.class);
            cfg.addClass(Software.class);

            SESSION_FACTORY = cfg.buildSessionFactory();
        } catch (HibernateException ex) {
            LOGGER.error("Initial SessionFactory creation failed.", ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private HibernateUtil() {
    }

}
