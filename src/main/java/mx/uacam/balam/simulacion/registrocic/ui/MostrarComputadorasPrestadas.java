/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import mx.uacam.balam.simulacion.registrocic.data.HibernateUtil;
import mx.uacam.balam.simulacion.registrocic.data.entities.Alumno;
import mx.uacam.balam.simulacion.registrocic.data.entities.Computadora;
import mx.uacam.balam.simulacion.registrocic.data.entities.EstadoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPcId;
import mx.uacam.balam.simulacion.registrocic.data.entities.Sala;
import mx.uacam.balam.simulacion.registrocic.data.entities.Software;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 *
 * @author six
 */
public class MostrarComputadorasPrestadas extends javax.swing.JFrame {

    /** Creates new form MostrarComputadorasPrestadas */
    private MostrarComputadorasPrestadas() {
        initComponents();
        cargarSalas();
    }

    public static MostrarComputadorasPrestadas getInstance() {
        return SingletonHolder.instance;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jXHeader1.setMinimumSize(new java.awt.Dimension(212, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(217, 90));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jXHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cargarSalas() {
        try (Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query<Sala> consulta = session.createQuery("from Sala", Sala.class);
            List<Sala> salas = consulta.getResultList();
            salas.forEach((sala) -> {
                JPanel jPanel = new JPanel();
                for (Computadora computadora : sala.getComputadoras()) {
                    JButton jButton = new JButton(computadora.getSerie());

                    switch (computadora.getEstadoPc().getNombre()) {
                        case "ocupado":
                            jButton.setIcon(Images.IMAGEN_COMPUTADORA_USO.getIcon(32, 32));
                            break;
                        case "disponible":
                            jButton.setIcon(Images.IMAGEN_COMPUTADORA_DISPONIBLE.getIcon(32, 32));
                            break;
                        default:
                            jButton.setIcon(Images.IMAGEN_COMPUTADORA_REPARACION.getIcon(32, 32));
                            break;
                    }

                    jButton.setToolTipText("Estado: " + computadora.getEstadoPc().getNombre());
                    jButton.addActionListener((ActionEvent e) -> {
                        if (e.getSource() instanceof JButton) {
                            JButton jButton1 = (JButton) e.getSource();
                            liberarUsarComputadora(jButton1);
                        }
                    });

                    jPanel.add(computadora.getSerie(), jButton);
                }
                jTabbedPane1.add("Sala " + sala.getNombre(), jPanel);
            });
            transaction.commit();
        } catch (HibernateException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void liberarUsarComputadora(JButton botonComputadora) {
        String nombrePC = botonComputadora.getText();

        try(Session session = HibernateUtil.SESSION_FACTORY.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query<Computadora> consulta = session.createQuery("from Computadora as computadora where computadora.serie = :nombrePC", Computadora.class);
            consulta.setParameter("nombrePC", nombrePC);
            Computadora computadora = consulta.getSingleResult();
            switch (computadora.getEstadoPc().getNombre()) {
                case "disponible":
                    usarComputadora(computadora);
                    botonComputadora.setIcon(Images.IMAGEN_COMPUTADORA_USO.getIcon(32, 32));
                    break;
                case "ocupado":
                    liberarComputadora(computadora);
                    botonComputadora.setIcon(Images.IMAGEN_COMPUTADORA_DISPONIBLE.getIcon(32, 32));
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Esta computadora no puede ser usada", "Computadora no disponible", JOptionPane.ERROR_MESSAGE);
                    break;
            }
            transaction.commit();
        } catch (HibernateException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void liberarComputadora(Computadora computadora) {
        try (Session session = HibernateUtil.SESSION_FACTORY.openSession();) {
            Transaction transaction = session.beginTransaction();
            Query<PrestamoPc> consulta = session.createQuery("from PrestamoPc as pres where pres.computadora.serie = :serie and pres.horaFin = null and pres.computadora.sala.nombre = nombreSala", PrestamoPc.class);//NOI18N
            consulta.setParameter("serie", computadora.getSerie());
            consulta.setParameter("nombreSala", computadora.getSala().getNombre());
            PrestamoPc prestamoPc = consulta.getSingleResult();
            prestamoPc.setHoraFin(Calendar.getInstance().getTime());
            EstadoPc estadoPc = session.createQuery("from EstadoPc as estado where estado.nombre = \'disponible\'", EstadoPc.class).getResultList().get(0);
            prestamoPc.getComputadora().setEstadoPc(estadoPc);
            session.update(prestamoPc);
            transaction.commit();
            JOptionPane.showMessageDialog(null, "El equipo se ha liberado correctamente");
        } catch (HibernateException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    private void usarComputadora(Computadora computadora) {
        Integer matricula = 0;
        String softwareNombre = "";

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.SESSION_FACTORY.openSession();
            transaction = session.beginTransaction();
            Query consulta = null;

            do {
                matricula = Integer.valueOf(JOptionPane.showInputDialog("Ingrese la matricula del alumno"));
                consulta = session.createQuery("from Alumno as alumno where alumno.matricula = '" + matricula + '\'');//NOI18N
            } while (consulta.list().isEmpty());
            Alumno alumno = (Alumno) consulta.list().get(0);

            do {
                softwareNombre = JOptionPane.showInputDialog("Ingrese el un software");
                consulta = session.createQuery("from Software as software where software.nombre = '" + softwareNombre + '\'');//NOI18N
            } while (consulta.list().isEmpty());
            Software software = (Software) consulta.list().get(0);
            PrestamoPcId id = new PrestamoPcId(matricula, computadora.getClave(), Calendar.getInstance().getTime(), Calendar.getInstance().getTime());
            EstadoPc estadoPc = (EstadoPc) session.createQuery("from EstadoPc as estadoPc where estadoPc.nombre = 'ocupado'").list().get(0);
            computadora.setEstadoPc(estadoPc);
            PrestamoPc prestamo = new PrestamoPc(id, computadora, software, alumno);
            session.save(prestamo);
            JOptionPane.showMessageDialog(null, "El equipo se ha asignado correctamente");
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.commit();
            }
            if (session.isOpen()) {
                session.close();
            }
        }
    }
    private static final long serialVersionUID = 8878567970552201154L;
    private static final Logger LOGGER = LogManager.getLogger(MostrarComputadorasPrestadas.class.getName());

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    // End of variables declaration//GEN-END:variables

    private static class SingletonHolder {
        private final static MostrarComputadorasPrestadas instance = new MostrarComputadorasPrestadas();
    }
}
