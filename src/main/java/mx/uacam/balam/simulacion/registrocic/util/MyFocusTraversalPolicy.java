/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Freddy Barrera
 */
public class MyFocusTraversalPolicy extends FocusTraversalPolicy {

    private List<Component> order;

    public MyFocusTraversalPolicy(Component[] compoments) {
        this(Arrays.asList(compoments));
    }

    public MyFocusTraversalPolicy(List<Component> order) {
        this.order = order;
    }

    @Override
    public Component getComponentAfter(Container aContainer, Component aComponent) {
        int idx = (order.indexOf(aComponent) + 1) % order.size();
        return order.get(idx);
    }

    @Override
    public Component getComponentBefore(Container aContainer, Component aComponent) {
        int idx = order.indexOf(aComponent) - 1;
        if (idx < 0) {
            idx = order.size() - 1;
        }
        return order.get(idx);
    }

    @Override
    public Component getFirstComponent(Container aContainer) {
        return order.get(0);
    }

    @Override
    public Component getLastComponent(Container aContainer) {
        return order.get(order.size() - 1);
    }

    @Override
    public Component getDefaultComponent(Container aContainer) {
        return order.get(0);
    }
}
