/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Freddy Barrera
 */
public class FormListener implements ActionListener, ActionCommand, OperationConstants {
    private final BasicOperations bo;

    public FormListener(BasicOperations bo) {
        this.bo = bo;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String command = evt.getActionCommand();
        switch (command) {
            case COMMAND_ADD:
                bo.add();
                break;
            case COMMAND_FIND_EDIT:
                bo.find(BUSCAR | MODIFICAR);
                break;
            case COMMAND_FIND_DELETE:
                bo.find(BUSCAR | ELIMINAR);
                break;
            case COMMAND_CANCEL_FIND_EDIT:
                bo.cancelFind(MODIFICAR);
                break;
            case COMMAND_CANCEL_FIND_DELETE:
                bo.cancelFind(ELIMINAR);
                break;
            case COMMAND_EDIT:
                bo.edit();
                break;
            case COMMAND_DELETE:
                bo.delete();
                break;
            case COMMAND_UPDATE_TABLE:
                bo.updateTable();
                break;
            default:
                break;
        }
    }
}