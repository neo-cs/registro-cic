/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data;

/**
 * This enumerable contains all the scripts for help in the database creation.
 * 
 * @author Freddy Barrera
 */
public enum SqlScript {
    
    CREATE_TABLE_ALUMNO (
            "CREATE TABLE IF NOT EXISTS alumno ("
                + " matricula        int          NOT NULL,"
                + " nombre           varchar(20)  NOT NULL,"
                + " apellido_paterno varchar(20)  DEFAULT '',"
                + " apellido_materno varchar(20)  DEFAULT '',"
                + " especialidad     varchar(3)   NOT NULL,"
                + " semestre         varchar(2)   DEFAULT '',"
                + " calle            varchar(20)  NOT NULL,"
                + " numero           varchar(4)   NOT NULL,"
                + " colonia          varchar(20)  NOT NULL,"
                + " codigo_postal    varchar(5)   NOT NULL,"
                + "PRIMARY KEY (matricula));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_COMPUTADORA (
            "CREATE TABLE IF NOT EXISTS computadora ("
                + "clave             int          NOT NULL, "
                + "sala              varchar(2)   NOT NULL, "
                + "estado            int          NOT NULL, "
                + "localizacion      int          NOT NULL, "
                + "serie             varchar(12)  NOT NULL      UNIQUE, "
                + "PRIMARY KEY (clave));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_DIA (
            "CREATE TABLE IF NOT EXISTS dia ("
                + "nombre            varchar(9)   NOT NULL, "
                + "PRIMARY KEY (nombre));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_ESPECIALIDAD (
            "CREATE TABLE IF NOT EXISTS especialidad ("
                + "clave             varchar(3)   NOT NULL, "
                + "nombre            varchar(20)  NOT NULL, "
                + "PRIMARY KEY (clave));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_ESTADO_PC (
            "CREATE TABLE IF NOT EXISTS estado_pc ("
                + "clave             int          NOT NULL, "
                + "nombre            varchar(20)  NOT NULL     UNIQUE,"
                + "descripcion       varchar(150) DEFAULT '',"
                + "PRIMARY KEY (clave));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_LOCALIZACION (
            "CREATE TABLE IF NOT EXISTS localizacion ("
                + "clave              int          NOT NULL, "
                + "nombre             varchar(20)  NOT NULL     UNIQUE,"
                + "descripcion        varchar(150) DEFAULT '',"
                + "PRIMARY KEY (clave));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_MAESTRO (
            "CREATE TABLE IF NOT EXISTS maestro ("
                + "clave_maestro      varchar(8)   NOT NULL, "
                + "nombre             varchar(20)  NOT NULL, "
                + "apellido_paterno   varchar(20)  DEFAULT '',"
                + "apellido_materno   varchar(20)  DEFAULT '', "
                + "PRIMARY KEY (clave_maestro));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_PRESTAMO_PC (
            "CREATE TABLE IF NOT EXISTS prestamo_pc ("
                + "matricula          int          NOT NULL, "
                + "clave_pc           int          NOT NULL, "
                + "hora_inicio        time         NOT NULL, "
                + "hora_fin           time         DEFAULT NULL, "
                + "fecha              date         NOT NULL, "
                + "id_software        int          NOT NULL, "
                + "PRIMARY KEY (matricula,clave_pc,hora_inicio,fecha));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_PRESTAMO_SALA (
            "CREATE TABLE IF NOT EXISTS prestamo_sala ("
                + "sala               varchar(2)   NOT NULL, "
                + "clave_maestro      varchar(8)   NOT NULL, "
                + "hora_inicio        time         NOT NULL, "
                + "hora_fin           time         NOT NULL, "
                + "dia                varchar(9)   NOT NULL, "
                + "id_software        int          NOT NULL, "
                + "carrera            varchar(3)   NOT NULL, "
                + "PRIMARY KEY (sala,hora_inicio,hora_fin,dia));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_SALA (
            "CREATE TABLE IF NOT EXISTS sala ("
                + "nombre             varchar(2)   NOT NULL,"
                + "PRIMARY KEY (nombre));",
            SqlScript.CREATE_TABLE
    ),
    CREATE_TABLE_SOFTWARE (
            "CREATE TABLE IF NOT EXISTS software ("
                + "id_software        int          NOT NULL,"
                + "nombre             varchar(20)  NOT NULL,"
                + "PRIMARY KEY (id_software));",
            SqlScript.CREATE_TABLE
    ),
    ALTER_TABLE_ALUMNO (
            "ALTER TABLE alumno ADD CONSTRAINT IF NOT EXISTS al_esp FOREIGN KEY (especialidad)"
                    + " REFERENCES especialidad (clave) ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_COMPUTADORA_1 (
            "ALTER TABLE computadora ADD CONSTRAINT IF NOT EXISTS compu_estado"
                    + " FOREIGN KEY (estado) REFERENCES estado_pc (clave)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_COMPUTADORA_2 (
            "ALTER TABLE computadora ADD CONSTRAINT IF NOT EXISTS compu_localizacion"
                    + " FOREIGN KEY (localizacion) REFERENCES localizacion (clave)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_COMPUTADORA_3 (
            "ALTER TABLE computadora ADD CONSTRAINT IF NOT EXISTS compu_sala"
                    + " FOREIGN KEY (sala) REFERENCES sala (nombre)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_PC_1 (
            "ALTER TABLE prestamo_pc ADD CONSTRAINT IF NOT EXISTS pres_pc_clapc"
                    + " FOREIGN KEY (clave_pc) REFERENCES computadora (clave)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_PC_2 (
            "ALTER TABLE prestamo_pc ADD CONSTRAINT IF NOT EXISTS pres_pc_mat"
                    + " FOREIGN KEY (matricula) REFERENCES alumno (matricula)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_PC_3 (
            "ALTER TABLE prestamo_pc ADD CONSTRAINT IF NOT EXISTS pres_pc_soft"
                    + " FOREIGN KEY (id_software) REFERENCES software (id_software)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_SALA_1 (
            "ALTER TABLE prestamo_sala ADD CONSTRAINT IF NOT EXISTS pres_sala_carrera"
                    + " FOREIGN KEY (carrera) REFERENCES especialidad (clave)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_SALA_2 (
            "ALTER TABLE prestamo_sala ADD CONSTRAINT IF NOT EXISTS pres_sala_dia"
                    + " FOREIGN KEY (dia) REFERENCES dia (nombre)ON DELETE RESTRICT"
                    + " ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_SALA_3 (
            "ALTER TABLE prestamo_sala ADD CONSTRAINT IF NOT EXISTS pres_sala_mae"
                    + " FOREIGN KEY (clave_maestro) REFERENCES maestro (clave_maestro)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_SALA_4 (
            "ALTER TABLE prestamo_sala ADD CONSTRAINT IF NOT EXISTS pres_sala_sa"
                    + " FOREIGN KEY (sala) REFERENCES sala (nombre)"
                    + " ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    ALTER_TABLE_PRESTAMO_SALA_5 (
            "ALTER TABLE prestamo_sala ADD CONSTRAINT IF NOT EXISTS pres_sala_soft"
                    + " FOREIGN KEY (id_software) REFERENCES software (id_software)"
                    + "ON DELETE RESTRICT ON UPDATE RESTRICT;",
            SqlScript.ALTER_TABLE
    ),
    INSERT_ESTADO_PC_1 (
            "INSERT INTO estado_pc (clave, nombre, descripcion) VALUES"
            + "(1, 'Disponible', 'El equipo no está en uso');",
            SqlScript.INSERT_INTO_TABLE
    ),
    INSERT_ESTADO_PC_2 (
            "INSERT INTO estado_pc (clave, nombre, descripcion) VALUES"
            + "(2, 'Ocupado', 'El equipo está en uso');",
            SqlScript.INSERT_INTO_TABLE
    ),
    INSERT_LOCALIZACION (
            "INSERT INTO localizacion (clave, nombre) VALUES"
            + "(1, 'sala');",
            SqlScript.INSERT_INTO_TABLE
    ),
    EXIST_ESTADO_PC_1(
            "SELECT count(*) = 1 AS exist"
            + " FROM estado_pc"
            + " WHERE estado_pc.clave = 1"
            + " AND estado_pc.nombre = 'Disponible';",
            SqlScript.EXIST_TABLE_RECORD
    ),
    EXIST_ESTADO_PC_2(
            "SELECT count(*) = 1 AS exist"
            + " FROM estado_pc"
            + " WHERE estado_pc.clave = 2"
            + " AND estado_pc.nombre = 'Ocupado';",
            SqlScript.EXIST_TABLE_RECORD),
    HAS_LOCALIZACION(
            "SELECT count(*) > 0 AS tieneLocalizacion"
            + " FROM localizacion;",
            SqlScript.EXIST_TABLE_RECORD);
    
    public static final short CREATE_TABLE = 0;
    public static final short ALTER_TABLE = 1;
    public static final short INSERT_INTO_TABLE = 2;
    public static final short EXIST_TABLE_RECORD = 3;

    public String getScript() {
        return script;
    }

    public short getTypeScript() {
        return typeScript;
    }

    private SqlScript(String value, short typeScript) {
        this.script = value;
        this.typeScript = typeScript;
    }

    private final String script;
    private final short typeScript;
    
}
