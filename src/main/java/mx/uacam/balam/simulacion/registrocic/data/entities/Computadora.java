/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.entities;
// Generated 29-jun-2010 12:20:47 by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * Computadora generated by hbm2java
 */
public class Computadora  implements java.io.Serializable {

    private static final long serialVersionUID = 3558158695376913630L;
     private Short clave;
     private Localizacion localizacion;
     private EstadoPc estadoPc;
     private Sala sala;
     private String serie;
     private Set<PrestamoPc> prestamoPcs = new HashSet<>();

    public Computadora() {
    }


    public Computadora(Localizacion localizacion, EstadoPc estadoPc, Sala sala, String serie) {
        this.localizacion = localizacion;
        this.estadoPc = estadoPc;
        this.sala = sala;
        this.serie = serie;
    }
    public Computadora(Localizacion localizacion, EstadoPc estadoPc, Sala sala, String serie, Set<PrestamoPc> prestamoPcs) {
       this.localizacion = localizacion;
       this.estadoPc = estadoPc;
       this.sala = sala;
       this.serie = serie;
       this.prestamoPcs = prestamoPcs;
    }

    public Short getClave() {
        return this.clave;
    }

    public void setClave(Short clave) {
        this.clave = clave;
    }
    public Localizacion getLocalizacion() {
        return this.localizacion;
    }

    public void setLocalizacion(Localizacion localizacion) {
        this.localizacion = localizacion;
    }
    public EstadoPc getEstadoPc() {
        return this.estadoPc;
    }

    public void setEstadoPc(EstadoPc estadoPc) {
        this.estadoPc = estadoPc;
    }
    public Sala getSala() {
        return this.sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }
    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }
    public Set<PrestamoPc> getPrestamoPcs() {
        return this.prestamoPcs;
    }

    public void setPrestamoPcs(Set<PrestamoPc> prestamoPcs) {
        this.prestamoPcs = prestamoPcs;
    }

    @Override
    public String toString() {
        return serie;
    }
}