/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.ReportViewer;

/**
 *
 * @author Freddy Barrera
 */
public class VentanaAdministracion extends javax.swing.JFrame {

    /** Creates new form VentanaAdministracion */
    private VentanaAdministracion() {
        initComponents();
        loadActionsAdmin();
        loadActionsReports();
        loadActionsShare();
    }

    public static VentanaAdministracion getInstance() {
        return SingletonHolder.instance;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXTaskPaneContainer1 = new org.jdesktop.swingx.JXTaskPaneContainer();
        jXTaskPaneAdministracion = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPaneReporte = new org.jdesktop.swingx.JXTaskPane();
        jXTaskPanePrestamos = new org.jdesktop.swingx.JXTaskPane();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Adminitrador");

        jXTaskPaneContainer1.setMinimumSize(new java.awt.Dimension(190, 190));
        jXTaskPaneContainer1.setPreferredSize(new java.awt.Dimension(220, 190));

        jXTaskPaneAdministracion.setTitle("Administración");
        jXTaskPaneAdministracion.setIcon(Images.IMAGEN_ADMINISTRADOR.getIcon());
        jXTaskPaneContainer1.add(jXTaskPaneAdministracion);

        jXTaskPaneReporte.setIcon(Images.IMAGEN_REPORTE.getIcon(24, 24));
        jXTaskPaneReporte.setTitle("Reportes");
        jXTaskPaneContainer1.add(jXTaskPaneReporte);

        jXTaskPanePrestamos.setIcon(Images.IMAGEN_SALA.getIcon(24, 24));
        jXTaskPanePrestamos.setTitle("Prestamos");
        jXTaskPaneContainer1.add(jXTaskPanePrestamos);

        getContentPane().add(jXTaskPaneContainer1, java.awt.BorderLayout.WEST);

        jPanelAdministrarUsuario1 = new JPanelAdministrarAlumno();
        jPanelAdministrarComputadora1= new JPanelAdministrarComputadora();
        jPanelAdministrarSoftware1 = new JPanelAdministrarSoftware();
        jPanelAdministrarMaestro1 = new JPanelAdministrarMaestro();
        jPanelAdministrarSala1 = new JPanelAdministrarSala();
        jPanelAdministrarEspecialidad1 = new JPanelAdministrarEspecialidad();
        jPanelAdministrarEstadoPc1 = new JPanelAdministrarEstadoPc();
        jPanelAdministrarLocalizaciones1 = new JPanelAdministrarLocalizaciones();
        jPanelDetallePrestamos1 = new JPanelDetallePrestamos();
        jPanel1.setLayout(new java.awt.CardLayout());

        jPanel1.add(jPanelAdministrarUsuario1, "Administrar alumno");
        jPanel1.add(jPanelAdministrarComputadora1, "Administrar computadora");
        jPanel1.add(jPanelAdministrarSoftware1, "Administrar software");
        jPanel1.add(jPanelAdministrarMaestro1, "Administrar maestro");
        jPanel1.add(jPanelAdministrarSala1, "Administrar sala");
        jPanel1.add(jPanelAdministrarEspecialidad1, "Administrar especialidad");
        jPanel1.add(jPanelAdministrarEstadoPc1, "Administrar estado PC");
        jPanel1.add(jPanelAdministrarLocalizaciones1, "Administrar localizacion");
        jPanel1.add(jPanelDetallePrestamos1,"Detalle prestamos");

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension dialogSize = getSize();
        setLocation((screenSize.width-dialogSize.width)/2,(screenSize.height-dialogSize.height)/2);
    }// </editor-fold>//GEN-END:initComponents

    private void loadActionsAdmin() {
        jXTaskPaneAdministracion.add(new FormListener("Administrar alumno",
                "Le permite administrar un alumno",
                Images.IMAGEN_ALUMNO.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener("Administar maestro",
                "Le permite administrar un maestro",
                Images.IMAGEN_MAESTRO.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener(
                "Administrar especialidad",
                "Le permite administrar la especialidad",
                Images.IMAGEN_USUARIO.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener("Administrar software",
                "Le permite administrar el software",
                Images.IMAGEN_SOFTWARE.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener("Administrar computadora",
                "Le permite administrar una computadora",
                Images.IMAGEN_COMPUTADORAS.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener("Administrar sala",
                "Le permite administrar una sala de computo",
                Images.IMAGEN_SALA.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener(
                "Administrar estados de PC",
                "Le permite administrar un estado de una Pc",
                Images.IMAGEN_COMPUTADORAS.getIcon(24, 24)));

        jXTaskPaneAdministracion.add(new FormListener(
                "Administrar localizaciones",
                "Le permite administrar Localizaciones",
                Images.IMAGEN_COMPUTADORAS.getIcon(24, 24)));
    }

    private void loadActionsReports(){
        jXTaskPaneReporte.add(new FormListener("Generar reporte",
                "Le permite generar un reporte",
                Images.IMAGEN_REPORTE_GENERAR.getIcon(24, 24)));

        jXTaskPaneReporte.add(new FormListener("Ver reporte",
                "Le permite ver un reporte",
                Images.IMAGEN_REPORTE_VER.getIcon(24, 24)));

        jXTaskPaneReporte.add(new FormListener("Imprimir reporte",
                "Le permite mandar a impresion un reporte",
                Images.IMAGEN_IMPRESORA.getIcon(24, 24)));
    }

    private void loadActionsShare(){
        jXTaskPanePrestamos.add(new FormListener("Ver sala",
                "Le permite ver es estado de las computadoras",
                Images.IMAGEN_SALA.getIcon(24, 24)));

        jXTaskPanePrestamos.add(new FormListener("Ver prestamos a alumno",
                "Le permite ver el listado de prestamos",
                Images.IMAGEN_SALA.getIcon(24, 24)));

    }

    private static final long serialVersionUID = 1662678946770357911L;
    private JPanelAdministrarAlumno jPanelAdministrarUsuario1;
    private JPanelAdministrarComputadora jPanelAdministrarComputadora1;
    private JPanelAdministrarSoftware jPanelAdministrarSoftware1;
    private JPanelAdministrarMaestro jPanelAdministrarMaestro1;
    private JPanelAdministrarSala jPanelAdministrarSala1;
    private JPanelAdministrarEspecialidad jPanelAdministrarEspecialidad1;
    private JPanelAdministrarEstadoPc jPanelAdministrarEstadoPc1;
    private JPanelAdministrarLocalizaciones jPanelAdministrarLocalizaciones1;
    private JPanelDetallePrestamos jPanelDetallePrestamos1;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneAdministracion;
    private org.jdesktop.swingx.JXTaskPaneContainer jXTaskPaneContainer1;
    private org.jdesktop.swingx.JXTaskPane jXTaskPanePrestamos;
    private org.jdesktop.swingx.JXTaskPane jXTaskPaneReporte;
    // End of variables declaration//GEN-END:variables

    private static class SingletonHolder {
        private final static VentanaAdministracion instance = new VentanaAdministracion();
    }

    private final class FormListener extends AbstractAction{
        private static final long serialVersionUID = -9112172483734546812L;

        public FormListener(String name, String shortDescription, Icon smallIcon) {
            putValue(Action.NAME, name);
            putValue(Action.SHORT_DESCRIPTION, shortDescription);
            putValue(Action.SMALL_ICON, smallIcon);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            CardLayout cl = (CardLayout) (jPanel1.getLayout());

            if(command.equals("Administrar alumno")){
                cl.show(jPanel1, "Administrar alumno");
            } else if (command.equals("Administar maestro")){
                cl.show(jPanel1, "Administrar maestro");
            } else if (command.equals("Administrar especialidad")){
                cl.show(jPanel1, "Administrar especialidad");
            } else if (command.equals("Administrar software")){
                cl.show(jPanel1, "Administrar software");
            } else if (command.equals("Administrar computadora")){
                cl.show(jPanel1, "Administrar computadora");
            } else if (command.equals("Administrar sala")){
                cl.show(jPanel1, "Administrar sala");
            } else if (command.equals("Administrar software")){
                cl.show(jPanel1, "Administrar software");
            } else if (command.equals("Administrar sala")){
                cl.show(jPanel1, "Administrar sala");
            } else if (command.equals("Administrar estados de PC")){
                cl.show(jPanel1, "Administrar estado PC");
            } else if (command.equals("Administrar localizaciones")){
                cl.show(jPanel1, "Administrar localizacion");
            } else if (command.equals("Generar reporte")) {
                new ReportViewer().mostrarReporte();
            } else if (command.equals("Ver reporte")){
                System.out.println("Clic: Ver reporte");
            } else if (command.equals("Imprimir reporte")){
                System.out.println("Clic: Imprimir reporte");
            } else if (command.equals("Ver sala")){
                MostrarComputadorasPrestadas.getInstance().setVisible(true);
            } else if (command.equals("Ver prestamos a alumno")){
                cl.show(jPanel1, "Detalle prestamos");
            }
        }
    }
}