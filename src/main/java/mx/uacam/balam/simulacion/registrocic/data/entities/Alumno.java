/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.entities;
// Generated 29-jun-2010 12:20:47 by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;

/**
 * This class is an entity who represent the table alumno from the database.
 */
public class Alumno implements java.io.Serializable {

    private static final long serialVersionUID = -6311140123209342249L;
    private Integer matricula;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private Especialidad especialidad;
    private String semestre;
    private String calle;
    private String numero;
    private String colonia;
    private String codigoPostal;
    private Set<PrestamoPc> prestamoPcs = new HashSet<>();

    /**
     * Create a new intance from Alumno (student).
     */
    public Alumno() {
        this(0, "", new Especialidad(), "", "", "", "");
    }

    /**
     * Create a new intance from Alumno (student).
     * 
     * @param matricula the ID from the student.
     * @param nombre the first name from the student.
     * @param especialidad the area of studio.
     * @param calle the street where the student lives.
     * @param numero the number of student house.
     * @param colonia the the block name where the student lives.
     * @param codigoPostal the zip code.
     */
    public Alumno(Integer matricula, String nombre, Especialidad especialidad, String calle,
            String numero, String colonia, String codigoPostal) {
        this(matricula, nombre, "", "", especialidad, "", calle, numero, colonia, codigoPostal);
    }

    /**
     * Create a new intance from Alumno (student).
     *
     * @param matricula the ID from the student.
     * @param nombre the first name and middle name of the student.
     * @param apellidoPaterno the last name of the student.
     * @param apellidoMaterno the mothers maiden name of the student.
     * @param especialidad the area of studio.
     * @param semestre the semester of the student is coursing.
     * @param calle the street where the student lives.
     * @param numero the number of student house.
     * @param colonia the the block name where the student lives.
     * @param codigoPostal the zip code.
     */
    public Alumno(Integer matricula, String nombre, String apellidoPaterno, String apellidoMaterno,
            Especialidad especialidad, String semestre, String calle, String numero, String colonia,
            String codigoPostal) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.especialidad = especialidad;
        this.semestre = semestre;
        this.calle = calle;
        this.numero = numero;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
    }

    /**
     * Get the ID from the student.
     * 
     * @return the ID from the student.
     */
    public Integer getMatricula() {
        return matricula;
    }

    /**
     * Set the ID from the student.
     *
     * @param matricula the ID from the student
     */
    public void setMatricula(Integer matricula) {
        this.matricula = matricula;
    }

    /**
     * Get the area of studio.
     * 
     * @return the area of studio.
     */
    public Especialidad getEspecialidad() {
        return this.especialidad;
    }

    /**
     * Set the area of studio.
     * 
     * @param especialidad the area of studio.
     */
    public void setEspecialidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }

    /**
     * Get the first name and middle name of the student.
     * 
     * @return the first name and middle name of the student.
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Set the first name and middle name of the student.
     * 
     * @param nombre the first name and middle name of the student.
     */
    public void setNombre(String nombre) {
        if (nombre != null) {
            if (nombre.length() > 20 && nombre.length() < 3) {
                throw new IllegalArgumentException(
                        "El campo nombre no debe se mayor de 20 caracteres ni menor de 3.");
            }
            for (int i = 0; i < nombre.length(); i++) {
                if (!Character.isLetter(nombre.charAt(i))) {
                    throw new IllegalArgumentException(
                            "El campo nombre debe contener unicamente letras.");
                }
            }
        }
        this.nombre = nombre;
    }

    /**
     * Get the last name of the student.
     * 
     * @return the last name of the student.
     */
    public String getApellidoPaterno() {
        return this.apellidoPaterno;
    }

    /**
     * Set the last name of the student.
     * 
     * @param apellidoPaterno the last name of the student.
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        if (apellidoPaterno != null) {
            if (apellidoPaterno.length() > 20 && apellidoPaterno.length() < 3) {
                throw new IllegalArgumentException(
                        "El campo apellido paterno no debe se mayor de 20 caracteres ni menor de 3.");
            }
            for (int i = 0; i < apellidoPaterno.length(); i++) {
                if (!Character.isLetter(apellidoPaterno.charAt(i))) {
                    throw new IllegalArgumentException(
                            "El campo apellido paterno debe contener unicamente letras.");
                }
            }
        }
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * Get the mothers maiden name of the student.
     * 
     * @return the mothers maiden name of the student.
     */
    public String getApellidoMaterno() {
        return this.apellidoMaterno;
    }

    /**
     * Set the mothers maiden name of the student.
     * 
     * @param apellidoMaterno the mothers maiden name of the student.
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        if (apellidoMaterno != null) {
            if (apellidoMaterno.length() > 20 && apellidoMaterno.length() < 3) {
                throw new IllegalArgumentException(
                        "El campo apellido materno no debe se mayor de 20 caracteres ni menor de 3.");
            }
            for (int i = 0; i < apellidoMaterno.length(); i++) {
                if (!Character.isLetter(apellidoMaterno.charAt(i))) {
                    throw new IllegalArgumentException(
                            "El campo apellido materno debe contener unicamente letras.");
                }
            }
        }
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     *
     * @return
     */
    public String getSemestre() {
        return this.semestre;
    }

    /**
     *
     * @param semestre
     */
    public void setSemestre(String semestre) {
        if (semestre != null) {
            if (semestre.length() > 2) {
                throw new IllegalArgumentException(
                        "El campo semestre no debe mayor de 2 caracteres.");
            }
        }
        this.semestre = semestre;
    }

    /**
     *
     * @return
     */
    public String getCalle() {
        return this.calle;
    }

    /**
     *
     * @param calle
     */
    public void setCalle(String calle) {
        if (calle != null) {
            if (calle.length() > 20) {
                throw new IllegalArgumentException(
                        "El campo calle no debe mayor de 20 caracteres.");
            }
        }
        this.calle = calle;
    }

    /**
     *
     * @return
     */
    public String getNumero() {
        return this.numero;
    }

    /**
     *
     * @param numero
     */
    public void setNumero(String numero) {
        if (numero != null) {
            if (numero.length() > 4) {
                throw new IllegalArgumentException(
                        "El campo número no debe mayor de 4 caracteres.");
            }
        }
        this.numero = numero;
    }

    /**
     *
     * @return
     */
    public String getColonia() {
        return this.colonia;
    }

    /**
     *
     * @param colonia
     */
    public void setColonia(String colonia) {
        if (colonia != null) {
            if (colonia.length() > 20) {
                throw new IllegalArgumentException(
                        "El campo colonia no debe mayor de 20 caracteres.");
            }
        }
        this.colonia = colonia;
    }

    /**
     *
     * @return
     */
    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    /**
     *
     * @param codigoPostal
     */
    public void setCodigoPostal(String codigoPostal) {
        if (codigoPostal != null) {
            if (codigoPostal.length() != 5) {
                throw new IllegalArgumentException(
                        "El campo codigo postal deben  ser 5 caracteres.");
            }
            for (int i = 0; i < 5; i++) {
                if (!Character.isDigit(codigoPostal.charAt(i))) {
                    throw new IllegalArgumentException(
                            "El campo codigo postal debe contener unicamente números.");
                }
            }
        }
        this.codigoPostal = codigoPostal;
    }

    /**
     *
     * @return
     */
    public Set<PrestamoPc> getPrestamoPcs() {
        return this.prestamoPcs;
    }

    /**
     *
     * @param prestamoPcs
     */
    public void setPrestamoPcs(Set<PrestamoPc> prestamoPcs) {
        this.prestamoPcs = prestamoPcs;
    }

    @Override
    public String toString() {
        return "Alumno{"
                + "matricula=" + matricula
                + " nombre=" + nombre
                + " apellidoPaterno="+ apellidoPaterno
                + " apellidoMaterno=" + apellidoMaterno
                + '}';
    }
}
