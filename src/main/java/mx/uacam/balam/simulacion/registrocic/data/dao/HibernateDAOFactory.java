/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.dao;

import mx.uacam.balam.simulacion.registrocic.data.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author Freddy Barrera
 */
public class HibernateDAOFactory extends DAOFactory {

    private <T extends GenericHibernateDAO<?,?>> T instantiateDAO(Class<T> daoClass) {
        try {
            T dao = daoClass.newInstance();
            dao.setSession(getCurrentSession());
            return dao;
        } catch (IllegalAccessException | InstantiationException ex) {
            throw new RuntimeException("Can not instantiate DAO: " + daoClass, ex);
        }
    }

    // You could override this if you don't want HibernateUtil for lookup
    protected Session getCurrentSession() {
        return HibernateUtil.SESSION_FACTORY.openSession();
    }

    @Override
    public AlumnoDAO getAlumnoDAO() {
        return instantiateDAO(AlumnoDAOHibernate.class);
    }

    @Override
    public EspecialidadDAO getEspecialidadDAO() {
        return instantiateDAO(EspecialidadDAOHibernate.class);
    }

    @Override
    public SoftwareDAO getSoftwareDAO() {
        return instantiateDAO(SoftwareDAOHibernate.class);
    }

    @Override
    public ComputadoraDAO getComputadoraDAO() {
        return instantiateDAO(ComputadoraDAOHibernate.class);
    }

    @Override
    public EstadoPcDAO getEstadoPcDAO() {
        return instantiateDAO(EstadoPcDAOHibernate.class);
    }

    @Override
    public LocalizacionDAO getLocalizacionDAO() {
        return instantiateDAO(LocalizacionDAOHibernate.class);
    }

    @Override
    public MaestroDAO getMaestroDAO() {
        return instantiateDAO(MaestroDAOHibernate.class);
    }

    @Override
    public SalaDAO getSalaDAO(){
        return instantiateDAO(SalaDAOHibernate.class);
    }

    @Override
    public PrestamoPcDAO getPrestamoPcDAO() {
        return instantiateDAO(PrestamoPcDAOHibernate.class);
    }

}
