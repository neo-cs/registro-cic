/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import mx.uacam.balam.simulacion.registrocic.data.HyperSqlServer;
import mx.uacam.balam.simulacion.registrocic.data.dao.AlumnoDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.ComputadoraDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.EstadoPcDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.PrestamoPcDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.SoftwareDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Alumno;
import mx.uacam.balam.simulacion.registrocic.data.entities.Computadora;
import mx.uacam.balam.simulacion.registrocic.data.entities.EstadoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.PrestamoPcId;
import mx.uacam.balam.simulacion.registrocic.data.entities.Software;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Freddy Barrera
 */
public class VentanaAcceso extends javax.swing.JFrame {

    /**
     * Contructor de la clase por defecto
     */
    private VentanaAcceso() {
        HyperSqlServer.start();

        alumnoDAO = OperationConstants.FACTORY.getAlumnoDAO();
        computadoraDAO = OperationConstants.FACTORY.getComputadoraDAO();
        estadoPcDAO = OperationConstants.FACTORY.getEstadoPcDAO();
        softwareDAO = OperationConstants.FACTORY.getSoftwareDAO();
        prestamoPcDAO = OperationConstants.FACTORY.getPrestamoPcDAO();

        initComponents();

        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = getSize();

        setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
    }

    /**
     * Inicializa los componentes
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        backgroundJPanel1 = new mx.neocs.beans.panel.JBackgroundImagePanel();
        fondoJPanel1 = new mx.neocs.beans.panel.JBackgroundPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jXStatusBar1 = new org.jdesktop.swingx.JXStatusBar();
        statusJLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Prestamo de computadora");
        setMinimumSize(new java.awt.Dimension(464, 400));
        setName("VentanaAcceso"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(30, 64, 100));

        backgroundJPanel1.setBackgroundImageIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/banner_uac_avanza.png"))); // NOI18N
        backgroundJPanel1.setMinimumSize(new java.awt.Dimension(10, 150));

        fondoJPanel1.setBackground(java.awt.Color.white);
        fondoJPanel1.setAlpha(255);
        fondoJPanel1.setRadius(25);

        jLabel1.setFont(new java.awt.Font("Bookman Old Style", 1, 26)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 51, 102));
        jLabel1.setText("Bienvenido");

        jLabel2.setText("Matricula:");

        jFormattedTextField1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        jFormattedTextField1.setToolTipText("");

        jLabel3.setText("Software:");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });
        jComboBox1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jComboBox1FocusGained(evt);
            }
        });

        jButton1.setText("Usar equipo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usarEquipo(evt);
            }
        });

        jButton2.setText("Liberar equipo");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                liberarEquipo(evt);
            }
        });

        javax.swing.GroupLayout fondoJPanel1Layout = new javax.swing.GroupLayout(fondoJPanel1);
        fondoJPanel1.setLayout(fondoJPanel1Layout);
        fondoJPanel1Layout.setHorizontalGroup(
            fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fondoJPanel1Layout.createSequentialGroup()
                .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(fondoJPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE))
                    .addGroup(fondoJPanel1Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, fondoJPanel1Layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)))))
                .addContainerGap())
        );
        fondoJPanel1Layout.setVerticalGroup(
            fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fondoJPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(35, 35, 35)
                .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(fondoJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(fondoJPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(backgroundJPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backgroundJPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fondoJPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        statusJLabel.setText("Listo");
        jXStatusBar1.add(statusJLabel);

        jMenu1.setText("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salir(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edición");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Administrar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iniciarAdministrador(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");

        jMenuItem3.setText("Acerca de");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarAcercaDe(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXStatusBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXStatusBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void usarEquipo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usarEquipo
        if (seleccionoSoftware) {
            try {
                Integer matricula = Integer.valueOf(jFormattedTextField1.getText());
                if(!prestamoPcDAO.tienePrestamo(matricula)) {
                    Alumno alumno = alumnoDAO.findById(matricula, false);
                    Computadora computadora = new Computadora();
                    EstadoPc estado = estadoPcDAO.findById((short) 1, false);
                    computadora.setEstadoPc(estado);
                    List<Computadora> computadorasDisponibles = computadoraDAO.findByExample(computadora, new String[]{"EstadoPc"});
                    if(computadorasDisponibles != null && !computadorasDisponibles.isEmpty()) {
                        Random rnd = new Random(computadorasDisponibles.size());
                        EstadoPc estadoOcupado = estadoPcDAO.findById((short) 2, false);
                        Computadora computadoraAsignada = computadorasDisponibles.get(rnd.nextInt());
                        computadoraAsignada.setEstadoPc(estadoOcupado);
                        Date fecha = Calendar.getInstance().getTime();
                        PrestamoPcId prestamoPcId = new PrestamoPcId(matricula, computadora.getClave(), fecha, fecha);
                        PrestamoPc prestamo = new PrestamoPc(prestamoPcId, computadora, (Software) jComboBox1.getSelectedItem(), alumno);
                        try {
                            prestamoPcDAO.makePersistent(prestamo);
                        } catch (DuplicateEntryException ex) {
                            LOGGER.error(ex.getMessage(), ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Todos los equipos se encuentran ocupados,"
                                        + " por favor regrese en un momento en"
                                        + " lo que se desocupa uno, gracias",
                                "No hay equipos disponibles",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    liberarEquipo(evt);
                }

            } catch (EntryNotFoundException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }//GEN-LAST:event_usarEquipo

    private void iniciarAdministrador(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_iniciarAdministrador
        statusJLabel.setText("Cargando ventana de administrador...");
        Thread t = new Thread() {

            @Override
            public void run() {
                statusJLabel.setText("Listo");
            }
        };
        
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                v = VentanaAdministracion.getInstance();
                v.setVisible(true);
            }
        });
        
        t.start();
    }//GEN-LAST:event_iniciarAdministrador

    private void jComboBox1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jComboBox1FocusGained
        List<Software> softwareDisponible = softwareDAO.findAll();
        if (softwareDisponible.isEmpty()){
            softwareDisponible.add(new Software("No se ha encontrado alguna especialidad"));
        }
        jComboBox1.setModel(new DefaultComboBoxModel<>(softwareDisponible.toArray(new Software[softwareDisponible.size()])));
        //AutoCompleteDecorator.decorate(jComboBox1);//Linea comentada porque no permite actualizar la lista
    }//GEN-LAST:event_jComboBox1FocusGained

    private void mostrarAcercaDe(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrarAcercaDe
        new JDialogAcercaDe(this, true).setVisible(true);
    }//GEN-LAST:event_mostrarAcercaDe

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        seleccionoSoftware = true;
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void liberarEquipo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_liberarEquipo
        Integer matricula = Integer.valueOf(jFormattedTextField1.getText());
        if (prestamoPcDAO.liberar(matricula)) {
            JOptionPane.showMessageDialog(null, "El equipo se ha liberado correctamente");
        } else {
            JOptionPane.showMessageDialog(null, "El equipo no se ha logrado liberar");
        }
    }//GEN-LAST:event_liberarEquipo

    private void salir(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salir
        if (v != null) {
            v.setVisible(false);
            v = null;
        }

        setVisible(false);
        HyperSqlServer.shutdown();
        System.exit(0);
    }//GEN-LAST:event_salir

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (v != null) {
            v.setVisible(false);
            v = null;
        }
        setVisible(false);
        HyperSqlServer.shutdown();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            if (System.getProperty("os.name").contains("Windows")) {//NOI18N
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");//NOI18N
            } else {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");//NOI18N
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        EventQueue.invokeLater(() -> {
            VentanaAcceso.getInstance().setVisible(true);
        });
    }

    public static VentanaAcceso getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static final long serialVersionUID = -8764302563354031649L;
    private static final Logger LOGGER = LogManager.getLogger(VentanaAcceso.class.getName());

    private final AlumnoDAO alumnoDAO;
    private final ComputadoraDAO computadoraDAO;
    private final EstadoPcDAO estadoPcDAO;
    private final SoftwareDAO softwareDAO;
    private final PrestamoPcDAO prestamoPcDAO;

    private boolean seleccionoSoftware = false;
    private VentanaAdministracion v;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private mx.neocs.beans.panel.JBackgroundImagePanel backgroundJPanel1;
    private mx.neocs.beans.panel.JBackgroundPanel fondoJPanel1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<Software> jComboBox1;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private org.jdesktop.swingx.JXStatusBar jXStatusBar1;
    private javax.swing.JLabel statusJLabel;
    // End of variables declaration//GEN-END:variables

    private static class SingletonHolder {
        private final static VentanaAcceso INSTANCE = new VentanaAcceso();
    }
}
