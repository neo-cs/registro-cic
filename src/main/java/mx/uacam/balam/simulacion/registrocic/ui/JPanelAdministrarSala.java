/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.util.List;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.SalaDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Sala;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelSala;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.FormListener;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;
import static mx.uacam.balam.simulacion.registrocic.util.OperationConstants.REGISTRAR;
import static mx.uacam.balam.simulacion.registrocic.util.OperationConstants.bundle;

/**
 *
 * @author Freddy Barrera
 */
public class JPanelAdministrarSala extends javax.swing.JPanel implements OperationConstants, BasicOperations {
//TODO No se han implementados los métodos add(), edit(), delete(), etc...
    public JPanelAdministrarSala() {
        salaDAO = FACTORY.getSalaDAO();
        formListener = new FormListener(this);
        initComponents();
        jXHeader1.setIcon(Images.IMAGEN_SALA.getIcon());
        jXHeader1.setTitle("Administrar sala");
        jXHeader1.setDescription("Esta opción le permite añadir, eliminar"
                + " y visualizar las salas.");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textRegistrarNombreSala = new javax.swing.JTextField();
        botonRegistrarSala = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        textBuscarEliminarSala = new javax.swing.JTextField();
        botonBuscarEliminarSala = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        panelAceptarEliminarSala = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jLabel1.setText("Nombre:");

        botonRegistrarSala.setText("Registrar");
        botonRegistrarSala.setActionCommand(COMMAND_ADD);
        botonRegistrarSala.addActionListener(formListener);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textRegistrarNombreSala, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonRegistrarSala))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textRegistrarNombreSala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonRegistrarSala)
                .addContainerGap(128, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Registar", mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_EDITAR.getIcon(24, 24), jPanel1);

        jLabel14.setText("Nombre:");

        botonBuscarEliminarSala.setText("Buscar");
        botonBuscarEliminarSala.setActionCommand(COMMAND_FIND_DELETE);
        botonBuscarEliminarSala.addActionListener(formListener);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarEliminarSala, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBuscarEliminarSala)
                .addGap(77, 77, 77))
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(textBuscarEliminarSala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarEliminarSala))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel19.setText("Nombre:");

        jButton7.setText("Eliminar");
        jButton7.setActionCommand(COMMAND_DELETE);
        jButton7.setEnabled(false);
        jButton7.addActionListener(formListener);

        jLabel15.setText("-----------");

        javax.swing.GroupLayout panelAceptarEliminarSalaLayout = new javax.swing.GroupLayout(panelAceptarEliminarSala);
        panelAceptarEliminarSala.setLayout(panelAceptarEliminarSalaLayout);
        panelAceptarEliminarSalaLayout.setHorizontalGroup(
            panelAceptarEliminarSalaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAceptarEliminarSalaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAceptarEliminarSalaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAceptarEliminarSalaLayout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton7))
                .addContainerGap(268, Short.MAX_VALUE))
        );
        panelAceptarEliminarSalaLayout.setVerticalGroup(
            panelAceptarEliminarSalaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAceptarEliminarSalaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAceptarEliminarSalaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton7)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panelAceptarEliminarSala, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelAceptarEliminarSala, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Eliminar", mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanel3);

        jToolBar1.setRollover(true);

        jButton1.setIcon(Images.IMAGEN_ACTUALIZAR.getIcon(20, 20));
        jButton1.setText("Actualizar");
        jButton1.setToolTipText("Actualizar");
        jButton1.setActionCommand(COMMAND_UPDATE_TABLE);
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(formListener);
        jToolBar1.add(jButton1);

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelSala(salaDAO.findAll()));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Catálogo", mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jXHeader1, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 457, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jXHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void add() {
        if (isEmptyFields(REGISTRAR)) {
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.emptyRequestFields.message"), //NOI18N
                    bundle.getString("dialog.emptyRequestFields.title"), //NOI18N
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            Sala sala = new Sala(textRegistrarNombreSala.getText());
            try {
                salaDAO.makePersistent(sala);
                JOptionPane.showMessageDialog(null, "La sala se ha registrado correctamente", "Inserción completada", JOptionPane.INFORMATION_MESSAGE);
                cleanFields(REGISTRAR);
                updateTable();
            } catch (DuplicateEntryException ex) {

            }
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void delete() {
        try {
            Sala sala = salaDAO.findById(jLabel15.getText(), true);
            salaDAO.makeTransient(sala);
            JOptionPane.showMessageDialog(null, "La sala se ha eliminado correctamente", "Eliminación completada", JOptionPane.INFORMATION_MESSAGE);
            cleanFields(ELIMINAR);
            updateTable();
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "La sala no se ha podido eliminar", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void find(int action) {
        switch (action) {
            case BUSCAR | ELIMINAR :
                if (!isEmptyFields(action)) {
                    try {
                        Sala sala = salaDAO.findById(textBuscarEliminarSala.getText(), true);
                        jLabel15.setText(sala.getNombre());
                        jButton7.setEnabled(true);
                        cleanFields(action);
                    } catch (EntryNotFoundException ex) {
                        JOptionPane.showMessageDialog(null, bundle.getString("dialog.entityNotFound.message") + ex.getMessage() , bundle.getString("dialog.entityNotFound.title"), JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
        }
    }

    @Override
    public void cancelFind(int action) {
        switch (action) {
            case MODIFICAR:
                cleanFields(MODIFICAR);
                break;
            case ELIMINAR:
                cleanFields(ELIMINAR);
                break;
        }
    }

    @Override
    public boolean isEmptyFields(int action) {
        switch (action) {
            case REGISTRAR:
                return textRegistrarNombreSala.getText().isEmpty();
            case BUSCAR | ELIMINAR:
                return textBuscarEliminarSala.getText().isEmpty();
            default:
                throw new IllegalArgumentException("Input: " + action);
        }
    }

    @Override
    public void cleanFields(int action) {
        switch(action) {
            case REGISTRAR:
                textRegistrarNombreSala.setText("");
                break;
            case ELIMINAR:
                textBuscarEliminarSala.setText("");
                jLabel15.setText("----------------");
                break;
            case BUSCAR |ELIMINAR:
                textBuscarEliminarSala.setText("");
                break;
        }

    }

    @Override
    public void updateTable() {
        List<Sala> salas = salaDAO.findAll();
        TableModelSala tableModelSala = (TableModelSala) jTable1.getModel();
        tableModelSala.updateModel(salas);
    }

    private static final long serialVersionUID = -50677767997348120L;
    private final FormListener formListener;
    private final SalaDAO salaDAO;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscarEliminarSala;
    private javax.swing.JButton botonRegistrarSala;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JPanel panelAceptarEliminarSala;
    private javax.swing.JTextField textBuscarEliminarSala;
    private javax.swing.JTextField textRegistrarNombreSala;
    // End of variables declaration//GEN-END:variables

}