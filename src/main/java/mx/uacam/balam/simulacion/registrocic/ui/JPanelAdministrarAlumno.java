/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.AlumnoDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.EspecialidadDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Alumno;
import mx.uacam.balam.simulacion.registrocic.data.entities.Especialidad;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelAlumno;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.MyFocusTraversalPolicy;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;

/**
 *
 * @author Freddy Barrera
 */
public class JPanelAdministrarAlumno extends javax.swing.JPanel implements OperationConstants, BasicOperations {

    public JPanelAdministrarAlumno() {
        alumnoDAO = FACTORY.getAlumnoDAO();
        especialidadDAO = FACTORY.getEspecialidadDAO();
        formListener = new FormListener(this);
        loadEspecialidades();
        initComponents();
        loadFocusTraversalPolicy();
    }

    /**
     * This method initializes the form components
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelRegistrar = new javax.swing.JPanel();
        jLabelMatriculaRegistar = new javax.swing.JLabel();
        jLabelNombreRegistrar = new javax.swing.JLabel();
        jLabelApellidoPaternoRegistrar = new javax.swing.JLabel();
        jLabelApellidoMaternoRegistar = new javax.swing.JLabel();
        jLabelSemestreRegistrar = new javax.swing.JLabel();
        jLabelEspecialidadRegistrar = new javax.swing.JLabel();
        jLabelCalleRegistrar = new javax.swing.JLabel();
        jLabelNumeroRegistar = new javax.swing.JLabel();
        jLabelColoniaRegistar = new javax.swing.JLabel();
        jLabelCodigoPostalRegistar = new javax.swing.JLabel();
        jTextFieldMatriculaRegistrar = new javax.swing.JTextField();
        jTextFieldNombreRegistrar = new javax.swing.JTextField();
        jTextFieldApellidoPaternoRegistrar = new javax.swing.JTextField();
        jTextFieldApellidoMaternoRegistrar = new javax.swing.JTextField();
        jTextFieldSemestreRegistrar = new javax.swing.JTextField();
        jComboBoxEspecialidadRegistrar = new javax.swing.JComboBox<>();
        jTextFieldCalleRegistrar = new javax.swing.JTextField();
        jTextFieldNumeroRegistrar = new javax.swing.JTextField();
        jTextFieldColoniaRegistrar = new javax.swing.JTextField();
        jTextFieldCodigoPostalRegistrar = new javax.swing.JTextField();
        jButtonRegistrar = new javax.swing.JButton();
        jPanelModificar = new javax.swing.JPanel();
        jLabelMatriculaBuscarModificar = new javax.swing.JLabel();
        jLabelNombreModificar = new javax.swing.JLabel();
        jLabelApellidoParternoModificar = new javax.swing.JLabel();
        jLabelApellidoMarterno = new javax.swing.JLabel();
        jLabelEspecialidadModificar = new javax.swing.JLabel();
        jLabelSemestreModificar = new javax.swing.JLabel();
        jLabelCalleModificar = new javax.swing.JLabel();
        jLabelNumeroModificar = new javax.swing.JLabel();
        jLabelColoniaModificar = new javax.swing.JLabel();
        jLabelCodigoPostalModificar = new javax.swing.JLabel();
        jTextFieldMatriculaModificar = new javax.swing.JTextField();
        jTextFieldNombreModificar = new javax.swing.JTextField();
        jTextFieldApellidoPaternoModificar = new javax.swing.JTextField();
        jTextFieldApellidoMaternoModificar = new javax.swing.JTextField();
        jComboBoxEspecialidadModificar = new javax.swing.JComboBox<>();
        jTextFieldSemestreModificar = new javax.swing.JTextField();
        jTextFieldCalleModificar = new javax.swing.JTextField();
        jTextFieldNumeroModificar = new javax.swing.JTextField();
        jTextFieldColoniaModificar = new javax.swing.JTextField();
        jTextFieldCodigoPostalModificar = new javax.swing.JTextField();
        jButtonBuscarModificar = new javax.swing.JButton();
        jButtonModificar = new javax.swing.JButton();
        jButtonCancelarModificar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        labelMatriculaModificar = new javax.swing.JLabel();
        jLabelMatriculaModificar = new javax.swing.JLabel();
        jPanelEliminar = new javax.swing.JPanel();
        jLabelAlumnoNombreEliminar = new javax.swing.JLabel();
        jLabelAlumnoApellidoPaternoEliminar = new javax.swing.JLabel();
        jLabelAlumnoApellidoMaternoEliminar = new javax.swing.JLabel();
        jLabelAlumnoSemestreEliminar = new javax.swing.JLabel();
        jLabelAlumnoEspecialidadEliminar = new javax.swing.JLabel();
        jLabelAlumnoCalleEliminar = new javax.swing.JLabel();
        jLabelAlumnoNumeroEliminar = new javax.swing.JLabel();
        jLabelAlumnoColoniaEliminar = new javax.swing.JLabel();
        jLabelAlumnoCodigoPostalEliminar = new javax.swing.JLabel();
        jTextFieldMatriculaEliminar = new javax.swing.JTextField();
        jButtonBuscarEliminar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jLabelMatriculaBuscarEliminar = new javax.swing.JLabel();
        jLabelNombreEliminar = new javax.swing.JLabel();
        jLabelApellidoPaternoEliminar = new javax.swing.JLabel();
        jLabelApellidoMaternoEliminar = new javax.swing.JLabel();
        jLabelSemestreEliminar = new javax.swing.JLabel();
        jLabelEspecialidad = new javax.swing.JLabel();
        jLabelCalleEliminar = new javax.swing.JLabel();
        jLabelNumeroEliminar = new javax.swing.JLabel();
        jLabelColoniaEliminar = new javax.swing.JLabel();
        jLabelCodigoPostalEliminar = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jButtonCancelarEliminar = new javax.swing.JButton();
        LabelMatriculaEliminar = new javax.swing.JLabel();
        jLabelMatriculaEliminar = new javax.swing.JLabel();
        jPanelCatalogo = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar(bundle.getString("toolBar.updateTable"));
        jButtonActualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jXHeader1.setMinimumSize(new java.awt.Dimension(212, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(217, 90));
        jXHeader1.setIcon(Images.IMAGEN_ALUMNO.getIcon(64, 64));
        jXHeader1.setTitle("Administrar alumno");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
        jXHeader1.setDescription("Esta opción le permite añadir, modificar, eliminar y visualizar alumnos.");
        add(jXHeader1, java.awt.BorderLayout.PAGE_START);

        jPanelRegistrar.setFocusTraversalPolicyProvider(true);

        jLabelMatriculaRegistar.setText(bundle.getString("label.alumno.enrollment")); // NOI18N

        jLabelNombreRegistrar.setText(bundle.getString("label.alumno.name")); // NOI18N

        jLabelApellidoPaternoRegistrar.setText(bundle.getString("label.alumno.lastName")); // NOI18N

        jLabelApellidoMaternoRegistar.setText(bundle.getString("label.alumno.mothersMaidenName")); // NOI18N

        jLabelSemestreRegistrar.setText(bundle.getString("label.alumno.semester")); // NOI18N

        jLabelEspecialidadRegistrar.setText(bundle.getString("label.alumno.speciality")); // NOI18N

        jLabelCalleRegistrar.setText(bundle.getString("label.alumno.street")); // NOI18N

        jLabelNumeroRegistar.setText(bundle.getString("label.alumno.number")); // NOI18N

        jLabelColoniaRegistar.setText(bundle.getString("label.alumno.colony")); // NOI18N

        jLabelCodigoPostalRegistar.setText(bundle.getString("label.alumno.zip")); // NOI18N

        jComboBoxEspecialidadRegistrar.addFocusListener(formListener);

        jButtonRegistrar.setText(bundle.getString("button.add")); // NOI18N
        jButtonRegistrar.setActionCommand(COMMAND_ADD);
        jButtonRegistrar.addActionListener(formListener);

        javax.swing.GroupLayout jPanelRegistrarLayout = new javax.swing.GroupLayout(jPanelRegistrar);
        jPanelRegistrar.setLayout(jPanelRegistrarLayout);
        jPanelRegistrarLayout.setHorizontalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMatriculaRegistar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNombreRegistrar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoPaternoRegistrar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoMaternoRegistar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelSemestreRegistrar, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(10, 10, 10)
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldSemestreRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                            .addComponent(jTextFieldApellidoMaternoRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                            .addComponent(jTextFieldApellidoPaternoRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                            .addComponent(jTextFieldNombreRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                            .addComponent(jTextFieldMatriculaRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelCodigoPostalRegistar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelColoniaRegistar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNumeroRegistar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelCalleRegistrar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelEspecialidadRegistrar, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldCodigoPostalRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextFieldColoniaRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextFieldNumeroRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextFieldCalleRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jComboBoxEspecialidadRegistrar, 0, 212, Short.MAX_VALUE)))
                    .addComponent(jButtonRegistrar))
                .addContainerGap())
        );
        jPanelRegistrarLayout.setVerticalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelMatriculaRegistar)
                    .addComponent(jTextFieldMatriculaRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEspecialidadRegistrar)
                    .addComponent(jComboBoxEspecialidadRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombreRegistrar)
                    .addComponent(jTextFieldNombreRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCalleRegistrar)
                    .addComponent(jTextFieldCalleRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelApellidoPaternoRegistrar)
                    .addComponent(jTextFieldApellidoPaternoRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelNumeroRegistar)
                    .addComponent(jTextFieldNumeroRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelApellidoMaternoRegistar)
                    .addComponent(jTextFieldApellidoMaternoRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelColoniaRegistar)
                    .addComponent(jTextFieldColoniaRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSemestreRegistrar)
                    .addComponent(jTextFieldSemestreRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCodigoPostalRegistar)
                    .addComponent(jTextFieldCodigoPostalRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButtonRegistrar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("tab.add"), mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_EDITAR.getIcon(24, 24), jPanelRegistrar); // NOI18N

        jPanelModificar.setFocusTraversalPolicyProvider(true);

        jLabelMatriculaBuscarModificar.setText(bundle.getString("label.find")); // NOI18N

        jLabelNombreModificar.setText(bundle.getString("label.alumno.name")); // NOI18N

        jLabelApellidoParternoModificar.setText(bundle.getString("label.alumno.lastName")); // NOI18N

        jLabelApellidoMarterno.setText(bundle.getString("label.alumno.mothersMaidenName")); // NOI18N

        jLabelEspecialidadModificar.setText(bundle.getString("label.alumno.speciality")); // NOI18N

        jLabelSemestreModificar.setText(bundle.getString("label.alumno.semester")); // NOI18N

        jLabelCalleModificar.setText(bundle.getString("label.alumno.street")); // NOI18N

        jLabelNumeroModificar.setText(bundle.getString("label.alumno.number")); // NOI18N

        jLabelColoniaModificar.setText(bundle.getString("label.alumno.colony")); // NOI18N

        jLabelCodigoPostalModificar.setText(bundle.getString("label.alumno.zip")); // NOI18N

        jTextFieldMatriculaModificar.setToolTipText(bundle.getString("toolTip.alumno.find")); // NOI18N

        jTextFieldNombreModificar.setEditable(false);

        jTextFieldApellidoPaternoModificar.setEditable(false);

        jTextFieldApellidoMaternoModificar.setEditable(false);

        jComboBoxEspecialidadModificar.setEnabled(false);
        jComboBoxEspecialidadModificar.addFocusListener(formListener);

        jTextFieldSemestreModificar.setEditable(false);

        jTextFieldCalleModificar.setEditable(false);

        jTextFieldNumeroModificar.setEditable(false);

        jTextFieldColoniaModificar.setEditable(false);

        jTextFieldCodigoPostalModificar.setEditable(false);

        jButtonBuscarModificar.setText(bundle.getString("button.find")); // NOI18N
        jButtonBuscarModificar.setToolTipText(bundle.getString("button.find")); // NOI18N
        jButtonBuscarModificar.setActionCommand(COMMAND_FIND_EDIT);
        jButtonBuscarModificar.addActionListener(formListener);

        jButtonModificar.setText(bundle.getString("button.edit")); // NOI18N
        jButtonModificar.setActionCommand(COMMAND_EDIT);
        jButtonModificar.setEnabled(false);
        jButtonModificar.addActionListener(formListener);

        jButtonCancelarModificar.setText(bundle.getString("button.cancel")); // NOI18N
        jButtonCancelarModificar.setActionCommand(COMMAND_CANCEL_FIND_EDIT);
        jButtonCancelarModificar.addActionListener(formListener);

        labelMatriculaModificar.setText(bundle.getString("label.alumno.enrollment")); // NOI18N

        jLabelMatriculaModificar.setText(bundle.getString("label.line")); // NOI18N

        javax.swing.GroupLayout jPanelModificarLayout = new javax.swing.GroupLayout(jPanelModificar);
        jPanelModificar.setLayout(jPanelModificarLayout);
        jPanelModificarLayout.setHorizontalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
            .addGroup(jPanelModificarLayout.createSequentialGroup()
                .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelModificarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelSemestreModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoMarterno, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoParternoModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNombreModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelMatriculaModificar, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldSemestreModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(jTextFieldApellidoMaternoModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(jTextFieldApellidoPaternoModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(jTextFieldNombreModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                            .addComponent(jLabelMatriculaModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelCodigoPostalModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelColoniaModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNumeroModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelCalleModificar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelEspecialidadModificar, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldCodigoPostalModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(jTextFieldColoniaModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(jTextFieldNumeroModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(jTextFieldCalleModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                            .addComponent(jComboBoxEspecialidadModificar, 0, 177, Short.MAX_VALUE)))
                    .addGroup(jPanelModificarLayout.createSequentialGroup()
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelModificarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jButtonModificar))
                            .addGroup(jPanelModificarLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabelMatriculaBuscarModificar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldMatriculaModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonBuscarModificar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonCancelarModificar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelModificarLayout.setVerticalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelModificarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldMatriculaModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelMatriculaBuscarModificar)
                    .addComponent(jButtonBuscarModificar)
                    .addComponent(jButtonCancelarModificar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelModificarLayout.createSequentialGroup()
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelMatriculaModificar)
                            .addComponent(jLabelMatriculaModificar)
                            .addComponent(jLabelEspecialidadModificar)
                            .addComponent(jComboBoxEspecialidadModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldNombreModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelNombreModificar)
                            .addComponent(jLabelCalleModificar)
                            .addComponent(jTextFieldCalleModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelApellidoParternoModificar)
                            .addComponent(jTextFieldApellidoPaternoModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelNumeroModificar)
                            .addComponent(jTextFieldNumeroModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelApellidoMarterno)
                            .addComponent(jTextFieldApellidoMaternoModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelColoniaModificar)
                            .addComponent(jTextFieldColoniaModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31))
                    .addGroup(jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldSemestreModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelSemestreModificar)
                        .addComponent(jLabelCodigoPostalModificar)
                        .addComponent(jTextFieldCodigoPostalModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addComponent(jButtonModificar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("tab.edit"), mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_MODIFICAR.getIcon(24, 24), jPanelModificar); // NOI18N

        jLabelAlumnoNombreEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoApellidoPaternoEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoApellidoMaternoEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoSemestreEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoEspecialidadEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoCalleEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoNumeroEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoColoniaEliminar.setText(bundle.getString("label.line")); // NOI18N

        jLabelAlumnoCodigoPostalEliminar.setText(bundle.getString("label.line")); // NOI18N

        jTextFieldMatriculaEliminar.setToolTipText(bundle.getString("toolTip.alumno.find")); // NOI18N

        jButtonBuscarEliminar.setText(bundle.getString("button.find")); // NOI18N
        jButtonBuscarEliminar.setActionCommand(COMMAND_FIND_DELETE);
        jButtonBuscarEliminar.addActionListener(formListener);

        jButtonEliminar.setText(bundle.getString("button.delete")); // NOI18N
        jButtonEliminar.setActionCommand(COMMAND_DELETE);
        jButtonEliminar.setEnabled(false);
        jButtonEliminar.addActionListener(formListener);

        jLabelMatriculaBuscarEliminar.setText(bundle.getString("label.find")); // NOI18N

        jLabelNombreEliminar.setText(bundle.getString("label.alumno.name")); // NOI18N

        jLabelApellidoPaternoEliminar.setText(bundle.getString("label.alumno.lastName")); // NOI18N

        jLabelApellidoMaternoEliminar.setText(bundle.getString("label.alumno.mothersMaidenName")); // NOI18N

        jLabelSemestreEliminar.setText(bundle.getString("label.alumno.semester")); // NOI18N

        jLabelEspecialidad.setText(bundle.getString("label.alumno.speciality")); // NOI18N

        jLabelCalleEliminar.setText(bundle.getString("label.alumno.street")); // NOI18N

        jLabelNumeroEliminar.setText(bundle.getString("label.alumno.number")); // NOI18N

        jLabelColoniaEliminar.setText(bundle.getString("label.alumno.colony")); // NOI18N

        jLabelCodigoPostalEliminar.setText(bundle.getString("label.alumno.zip")); // NOI18N

        jButtonCancelarEliminar.setText(bundle.getString("button.cancel")); // NOI18N
        jButtonCancelarEliminar.setActionCommand(COMMAND_CANCEL_FIND_DELETE);
        jButtonCancelarEliminar.addActionListener(formListener);

        LabelMatriculaEliminar.setText(bundle.getString("label.alumno.enrollment")); // NOI18N

        jLabelMatriculaEliminar.setText(bundle.getString("label.line")); // NOI18N

        javax.swing.GroupLayout jPanelEliminarLayout = new javax.swing.GroupLayout(jPanelEliminar);
        jPanelEliminar.setLayout(jPanelEliminarLayout);
        jPanelEliminarLayout.setHorizontalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelEliminarLayout.createSequentialGroup()
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LabelMatriculaEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNombreEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoPaternoEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelApellidoMaternoEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelSemestreEliminar, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelAlumnoSemestreEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoApellidoMaternoEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoApellidoPaternoEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoNombreEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                            .addComponent(jLabelMatriculaEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelCodigoPostalEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelColoniaEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNumeroEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelCalleEliminar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelEspecialidad, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelAlumnoCalleEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoNumeroEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoColoniaEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoCodigoPostalEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(jLabelAlumnoEspecialidadEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE))
                        .addGap(46, 46, 46))
                    .addGroup(jPanelEliminarLayout.createSequentialGroup()
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                                .addComponent(jLabelMatriculaBuscarEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldMatriculaEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonBuscarEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonCancelarEliminar))
                            .addComponent(jButtonEliminar))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelEliminarLayout.setVerticalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldMatriculaEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelMatriculaBuscarEliminar)
                    .addComponent(jButtonBuscarEliminar)
                    .addComponent(jButtonCancelarEliminar))
                .addGap(8, 8, 8)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEliminarLayout.createSequentialGroup()
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelEliminarLayout.createSequentialGroup()
                                    .addComponent(jLabelCalleEliminar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelEliminarLayout.createSequentialGroup()
                                            .addComponent(jLabelNumeroEliminar)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jLabelColoniaEliminar)
                                            .addGap(25, 25, 25))
                                        .addComponent(jLabelCodigoPostalEliminar)))
                                .addGroup(jPanelEliminarLayout.createSequentialGroup()
                                    .addComponent(jLabelAlumnoCalleEliminar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabelAlumnoNumeroEliminar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabelAlumnoColoniaEliminar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabelAlumnoCodigoPostalEliminar)))
                            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                                .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelAlumnoEspecialidadEliminar)
                                    .addComponent(jLabelEspecialidad))
                                .addGap(95, 95, 95)))
                        .addGap(18, 18, 18)
                        .addComponent(jButtonEliminar))
                    .addGroup(jPanelEliminarLayout.createSequentialGroup()
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LabelMatriculaEliminar)
                            .addComponent(jLabelMatriculaEliminar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNombreEliminar)
                            .addComponent(jLabelAlumnoNombreEliminar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelApellidoPaternoEliminar)
                            .addComponent(jLabelAlumnoApellidoPaternoEliminar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelApellidoMaternoEliminar)
                            .addComponent(jLabelAlumnoApellidoMaternoEliminar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelSemestreEliminar)
                            .addComponent(jLabelAlumnoSemestreEliminar))))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("tab.delete"), mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanelEliminar); // NOI18N

        jPanelCatalogo.setLayout(new java.awt.BorderLayout());

        jToolBar1.setRollover(true);

        jButtonActualizar.setIcon(mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_ACTUALIZAR.getIcon(20, 20));
        jButtonActualizar.setText(bundle.getString("button.updateTable")); // NOI18N
        jButtonActualizar.setToolTipText(bundle.getString("toolTip.updateTable")); // NOI18N
        jButtonActualizar.setActionCommand(COMMAND_UPDATE_TABLE);
        jButtonActualizar.setFocusable(false);
        jButtonActualizar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonActualizar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonActualizar.addActionListener(formListener);
        jToolBar1.add(jButtonActualizar);

        jPanelCatalogo.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jTable1.setModel(new mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelAlumno(alumnoDAO.findAll()));
        jScrollPane1.setViewportView(jTable1);

        jPanelCatalogo.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("tab.catalog"), mx.uacam.balam.simulacion.registrocic.util.Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanelCatalogo); // NOI18N

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void add() {
        if (isEmptyFields(REGISTRAR)) {
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.emptyRequestFields.message"), //NOI18N
                    bundle.getString("dialog.emptyRequestFields.title"), //NOI18N
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            Alumno alumno = new Alumno(Integer.parseInt(jTextFieldMatriculaRegistrar.getText()),
                    jTextFieldNombreRegistrar.getText(),
                    jTextFieldApellidoPaternoRegistrar.getText(),
                    jTextFieldApellidoMaternoRegistrar.getText(),
                    (Especialidad) jComboBoxEspecialidadRegistrar.getModel().getSelectedItem(),
                    jTextFieldSemestreRegistrar.getText(),
                    jTextFieldCalleRegistrar.getText(),
                    jTextFieldNumeroRegistrar.getText(),
                    jTextFieldColoniaRegistrar.getText(),
                    jTextFieldCodigoPostalRegistrar.getText());

            try {
                alumnoDAO.makePersistent(alumno);
                JOptionPane.showMessageDialog(null, "El alumno se ha registrado correctamente", "Inserción completada", JOptionPane.INFORMATION_MESSAGE);
                cleanFields(REGISTRAR);
                updateTable();
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(null, bundle.getString("dialog.duplicateEntry.message") + ex.getMessage(), bundle.getString("dialog.duplicateEntry.title"), JOptionPane.ERROR_MESSAGE);
            } catch (RuntimeException ex) {
                JOptionPane.showMessageDialog(null, bundle.getString("dialog.unexpectedError.message") + ex.getMessage(), bundle.getString("dialog.unexpectedError.title"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void edit() {
        if (isEmptyFields(MODIFICAR)) {
            JOptionPane.showMessageDialog(null, "Los campos con * son obligatorios llenelos por favor", "Datos obligatotios vacios", JOptionPane.INFORMATION_MESSAGE);
        } else {
            try {
                Integer id = Integer.parseInt(jLabelMatriculaModificar.getText());
                Alumno alumno = alumnoDAO.findById(id, true);
                alumno.setEspecialidad((Especialidad) jComboBoxEspecialidadModificar.getSelectedItem());
                alumno.setNombre(jTextFieldNombreModificar.getText());
                alumno.setApellidoPaterno(jTextFieldApellidoPaternoModificar.getText());
                alumno.setApellidoMaterno(jTextFieldApellidoMaternoModificar.getText());
                alumno.setSemestre(jTextFieldSemestreModificar.getText());
                alumno.setCalle(jTextFieldCalleModificar.getText());
                alumno.setNumero(jTextFieldNumeroModificar.getText());
                alumno.setColonia(jTextFieldColoniaModificar.getText());
                alumno.setCodigoPostal(jTextFieldCodigoPostalModificar.getText());

                alumnoDAO.makePersistent(alumno);
                JOptionPane.showMessageDialog(null, "El alumno se ha modificado correctamente.", "Modificación completada", JOptionPane.INFORMATION_MESSAGE);
                cleanFields(MODIFICAR);
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Elemento duplicado", JOptionPane.ERROR_MESSAGE);
            } catch (EntryNotFoundException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Elemento no encontrado", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void find(int accion) {
        try {
            switch (accion) {
                case BUSCAR | MODIFICAR:
                    if(!isEmptyFields(BUSCAR | MODIFICAR)){
                        String matricula = jTextFieldMatriculaModificar.getText();
                        int id = Integer.parseInt(matricula);
                        Alumno alumno = alumnoDAO.findById(id, true);
                        jLabelMatriculaModificar.setText(alumno.getMatricula().toString());
                        jTextFieldNombreModificar.setText(alumno.getNombre());
                        jTextFieldApellidoPaternoModificar.setText(alumno.getApellidoPaterno());
                        jTextFieldApellidoMaternoModificar.setText(alumno.getApellidoMaterno());
                        jTextFieldSemestreModificar.setText(alumno.getSemestre());
                        jComboBoxEspecialidadModificar.setModel(new DefaultComboBoxModel<>(especialidades.toArray(new Especialidad[especialidades.size()])));
                        jComboBoxEspecialidadModificar.setSelectedItem(alumno.getEspecialidad());
                        jTextFieldCalleModificar.setText(alumno.getCalle());
                        jTextFieldNumeroModificar.setText(alumno.getNumero());
                        jTextFieldColoniaModificar.setText(alumno.getColonia());
                        jTextFieldCodigoPostalModificar.setText(alumno.getCodigoPostal());
                        cleanFields(BUSCAR | MODIFICAR);
                    } else {
                        JOptionPane.showMessageDialog(null, bundle.getString("dialog.emptyField.message"), bundle.getString("dialog.emptyField.title"), JOptionPane.WARNING_MESSAGE);
                    }
                    break;
                case BUSCAR | ELIMINAR:
                    if(!isEmptyFields(BUSCAR | ELIMINAR)){
                        String matricula = jTextFieldMatriculaEliminar.getText();
                        int id = Integer.parseInt(matricula);
                        Alumno alumno = alumnoDAO.findById(id, true);
                        jLabelMatriculaEliminar.setText(alumno.getMatricula().toString());
                        jLabelAlumnoNombreEliminar.setText(alumno.getNombre());
                        jLabelAlumnoApellidoPaternoEliminar.setText(alumno.getApellidoPaterno());
                        jLabelAlumnoApellidoMaternoEliminar.setText(alumno.getApellidoMaterno());
                        jLabelAlumnoSemestreEliminar.setText(alumno.getSemestre());
                        jLabelAlumnoEspecialidadEliminar.setText(alumno.getEspecialidad().getNombre());
                        jLabelAlumnoCalleEliminar.setText(alumno.getCalle());
                        jLabelAlumnoNumeroEliminar.setText(alumno.getNumero());
                        jLabelAlumnoColoniaEliminar.setText(alumno.getColonia());
                        jLabelAlumnoCodigoPostalEliminar.setText(alumno.getCodigoPostal());
                        cleanFields(BUSCAR | ELIMINAR);
                    } else {
                        JOptionPane.showMessageDialog(null, bundle.getString("dialog.emptyField.message"), bundle.getString("dialog.emptyField.title"), JOptionPane.WARNING_MESSAGE);
                    }
                    break;
            }
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, bundle.getString("dialog.entityNotFound.message") + ex.getMessage() , bundle.getString("dialog.entityNotFound.title"), JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void cancelFind(int fields) {
        switch (fields) {
            case MODIFICAR:
                cleanFields(MODIFICAR);
                break;
            case ELIMINAR:
                cleanFields(ELIMINAR);
                break;
        }
    }

    @Override
    public void delete() {
        try {
            int id = Integer.parseInt(jLabelMatriculaEliminar.getText());
            Alumno alumno = alumnoDAO.findById(id, true);
            alumnoDAO.makeTransient(alumno);
            JOptionPane.showMessageDialog(null, "El alumno se ha eliminado correctamente", "Eliminación completada", JOptionPane.INFORMATION_MESSAGE);
            cleanFields(ELIMINAR);
            updateTable();
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Alumno no eliminado", JOptionPane.ERROR_MESSAGE);
        } catch (RuntimeException ex) {
            JOptionPane.showMessageDialog(null, "No se ha completado la eliminación del alumno.\n" + ex.getMessage(), "Alumno no eliminado", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void updateTable() {
        List<Alumno> listaAlumnos = alumnoDAO.findAll();
        TableModelAlumno tm = (TableModelAlumno) jTable1.getModel();
        tm.updateModel(listaAlumnos);
    }

    @Override
    public void cleanFields(int accion) {
        switch (accion) {
            case REGISTRAR:
                jTextFieldApellidoMaternoRegistrar.setText("");
                jTextFieldApellidoPaternoRegistrar.setText("");
                jTextFieldCodigoPostalRegistrar.setText("");
                jTextFieldColoniaRegistrar.setText("");
                jTextFieldNombreRegistrar.setText("");
                jTextFieldNumeroRegistrar.setText("");
                jTextFieldMatriculaRegistrar.setText("");
                jTextFieldCalleRegistrar.setText("");
                jTextFieldSemestreRegistrar.setText("");
                jComboBoxEspecialidadRegistrar.setModel(new DefaultComboBoxModel<>());
                comboinactivoregistrar = true;
                break;
            case MODIFICAR:
                jLabelMatriculaModificar.setText("----------------");
                jTextFieldNombreModificar.setText("");
                jTextFieldApellidoPaternoModificar.setText("");
                jTextFieldApellidoMaternoModificar.setText("");
                jTextFieldSemestreModificar.setText("");
                jComboBoxEspecialidadModificar.setModel(new DefaultComboBoxModel<>());
                jTextFieldCalleModificar.setText("");
                jTextFieldNumeroModificar.setText("");
                jTextFieldColoniaModificar.setText("");
                jTextFieldCodigoPostalModificar.setText("");

                jTextFieldNombreModificar.setEditable(false);
                jTextFieldApellidoPaternoModificar.setEditable(false);
                jTextFieldApellidoMaternoModificar.setEditable(false);
                jTextFieldSemestreModificar.setEditable(false);
                jComboBoxEspecialidadModificar.setEnabled(false);
                jTextFieldCalleModificar.setEditable(false);
                jTextFieldNumeroModificar.setEditable(false);
                jTextFieldColoniaModificar.setEditable(false);
                jTextFieldCodigoPostalModificar.setEditable(false);
                jButtonModificar.setEnabled(false);
                comboinactivomodificar = true;
                break;
            case ELIMINAR:
                jLabelMatriculaEliminar.setText("----------------");
                jLabelAlumnoNombreEliminar.setText("----------------");
                jLabelAlumnoApellidoPaternoEliminar.setText("----------------");
                jLabelAlumnoApellidoMaternoEliminar.setText("----------------");
                jLabelAlumnoSemestreEliminar.setText("----------------");
                jLabelAlumnoEspecialidadEliminar.setText("----------------");
                jLabelAlumnoCalleEliminar.setText("----------------");
                jLabelAlumnoNumeroEliminar.setText("----------------");
                jLabelAlumnoColoniaEliminar.setText("----------------");
                jLabelAlumnoCodigoPostalEliminar.setText("----------------");
                jButtonEliminar.setEnabled(true);
                break;
            case BUSCAR | ELIMINAR:
                jTextFieldMatriculaEliminar.setEditable(true);
                jTextFieldMatriculaEliminar.setText("");
                jButtonEliminar.setEnabled(true);
                break;
            case BUSCAR | MODIFICAR:
                jTextFieldMatriculaModificar.setText("");
                jTextFieldNombreModificar.setEditable(true);
                jTextFieldApellidoPaternoModificar.setEditable(true);
                jTextFieldApellidoMaternoModificar.setEditable(true);
                jTextFieldSemestreModificar.setEditable(true);
                jComboBoxEspecialidadModificar.setEnabled(true);
                jTextFieldCalleModificar.setEditable(true);
                jTextFieldNumeroModificar.setEditable(true);
                jTextFieldColoniaModificar.setEditable(true);
                jTextFieldCodigoPostalModificar.setEditable(true);
                jButtonModificar.setEnabled(true);
                break;
            default:
                throw new IllegalArgumentException();
        }

    }

    @Override
    public boolean isEmptyFields(int field) {
        switch (field) {
            case REGISTRAR:
                return (jTextFieldNombreRegistrar.getText().isEmpty()
                        || jTextFieldMatriculaRegistrar.getText().isEmpty()
                        || jTextFieldCalleRegistrar.getText().isEmpty()
                        || jTextFieldNumeroRegistrar.getText().isEmpty()
                        || jTextFieldColoniaRegistrar.getText().isEmpty()
                        || jTextFieldCodigoPostalRegistrar.getText().isEmpty()
                        || comboinactivoregistrar);
            case MODIFICAR:
                return ("----------------".equals(jLabelMatriculaModificar.getText())
                        || jTextFieldNombreModificar.getText().isEmpty()
                        || jTextFieldCalleModificar.getText().isEmpty()
                        || jTextFieldNumeroModificar.getText().isEmpty()
                        || jTextFieldColoniaModificar.getText().isEmpty()
                        || jTextFieldCodigoPostalModificar.getText().isEmpty()
                        || comboinactivomodificar);
            case BUSCAR | MODIFICAR:
                return jTextFieldMatriculaModificar.getText().isEmpty();
            case BUSCAR |ELIMINAR:
                return jTextFieldMatriculaEliminar.getText().isEmpty();
            default:
                throw new IllegalArgumentException("Input: " + field);
        }
    }

    private void jComboBoxFocusGained(java.awt.event.FocusEvent evt) {
        loadEspecialidades();
        if (evt.getSource().equals(jComboBoxEspecialidadRegistrar)) {
            jComboBoxEspecialidadRegistrar.setModel(new DefaultComboBoxModel<>(especialidades.toArray(new Especialidad[especialidades.size()])));
            comboinactivoregistrar = false;
        } else if (evt.getSource().equals(jComboBoxEspecialidadModificar)) {
            jComboBoxEspecialidadModificar.setModel(new DefaultComboBoxModel<>(especialidades.toArray(new Especialidad[especialidades.size()])));
            comboinactivomodificar = false;
        }
    }

    private void loadEspecialidades() {
        especialidades = especialidadDAO.findAll();
    }

    private void loadFocusTraversalPolicy() {
        List<Component> listRegistrar = new ArrayList<>();
        List<Component> listModificar = new ArrayList<>();

        listRegistrar.add(jTextFieldMatriculaRegistrar);
        listRegistrar.add(jTextFieldNombreRegistrar);
        listRegistrar.add(jTextFieldApellidoPaternoRegistrar);
        listRegistrar.add(jTextFieldApellidoMaternoRegistrar);
        listRegistrar.add(jTextFieldSemestreRegistrar);
        listRegistrar.add(jComboBoxEspecialidadRegistrar);
        listRegistrar.add(jTextFieldCalleRegistrar);
        listRegistrar.add(jTextFieldNumeroRegistrar);
        listRegistrar.add(jTextFieldColoniaRegistrar);
        listRegistrar.add(jTextFieldCodigoPostalRegistrar);
        listRegistrar.add(jButtonRegistrar);
        listModificar.add(jTextFieldMatriculaModificar);
        listModificar.add(jButtonBuscarModificar);
        listModificar.add(jButtonCancelarModificar);
        listModificar.add(jTextFieldNombreModificar);
        listModificar.add(jTextFieldApellidoPaternoModificar);
        listModificar.add(jTextFieldApellidoMaternoModificar);
        listModificar.add(jComboBoxEspecialidadModificar);
        listModificar.add(jTextFieldSemestreModificar);
        listModificar.add(jTextFieldCalleModificar);
        listModificar.add(jTextFieldNumeroModificar);
        listModificar.add(jTextFieldColoniaModificar);
        listModificar.add(jTextFieldCodigoPostalModificar);
        listModificar.add(jButtonModificar);

        jPanelRegistrar.setFocusTraversalPolicy(new MyFocusTraversalPolicy(listRegistrar));
        jPanelModificar.setFocusTraversalPolicy(new MyFocusTraversalPolicy(listModificar));
    }

    private FormListener formListener;
    private boolean comboinactivoregistrar = true;
    private boolean comboinactivomodificar = true;
    private List<Especialidad> especialidades;
    private AlumnoDAO alumnoDAO;
    private EspecialidadDAO especialidadDAO;
    private static final long serialVersionUID = 3945842319070669312L;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelMatriculaEliminar;
    private javax.swing.JButton jButtonActualizar;
    private javax.swing.JButton jButtonBuscarEliminar;
    private javax.swing.JButton jButtonBuscarModificar;
    private javax.swing.JButton jButtonCancelarEliminar;
    private javax.swing.JButton jButtonCancelarModificar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonModificar;
    private javax.swing.JButton jButtonRegistrar;
    private javax.swing.JComboBox<Especialidad> jComboBoxEspecialidadModificar;
    private javax.swing.JComboBox<Especialidad> jComboBoxEspecialidadRegistrar;
    private javax.swing.JLabel jLabelAlumnoApellidoMaternoEliminar;
    private javax.swing.JLabel jLabelAlumnoApellidoPaternoEliminar;
    private javax.swing.JLabel jLabelAlumnoCalleEliminar;
    private javax.swing.JLabel jLabelAlumnoCodigoPostalEliminar;
    private javax.swing.JLabel jLabelAlumnoColoniaEliminar;
    private javax.swing.JLabel jLabelAlumnoEspecialidadEliminar;
    private javax.swing.JLabel jLabelAlumnoNombreEliminar;
    private javax.swing.JLabel jLabelAlumnoNumeroEliminar;
    private javax.swing.JLabel jLabelAlumnoSemestreEliminar;
    private javax.swing.JLabel jLabelApellidoMarterno;
    private javax.swing.JLabel jLabelApellidoMaternoEliminar;
    private javax.swing.JLabel jLabelApellidoMaternoRegistar;
    private javax.swing.JLabel jLabelApellidoParternoModificar;
    private javax.swing.JLabel jLabelApellidoPaternoEliminar;
    private javax.swing.JLabel jLabelApellidoPaternoRegistrar;
    private javax.swing.JLabel jLabelCalleEliminar;
    private javax.swing.JLabel jLabelCalleModificar;
    private javax.swing.JLabel jLabelCalleRegistrar;
    private javax.swing.JLabel jLabelCodigoPostalEliminar;
    private javax.swing.JLabel jLabelCodigoPostalModificar;
    private javax.swing.JLabel jLabelCodigoPostalRegistar;
    private javax.swing.JLabel jLabelColoniaEliminar;
    private javax.swing.JLabel jLabelColoniaModificar;
    private javax.swing.JLabel jLabelColoniaRegistar;
    private javax.swing.JLabel jLabelEspecialidad;
    private javax.swing.JLabel jLabelEspecialidadModificar;
    private javax.swing.JLabel jLabelEspecialidadRegistrar;
    private javax.swing.JLabel jLabelMatriculaBuscarEliminar;
    private javax.swing.JLabel jLabelMatriculaBuscarModificar;
    private javax.swing.JLabel jLabelMatriculaEliminar;
    private javax.swing.JLabel jLabelMatriculaModificar;
    private javax.swing.JLabel jLabelMatriculaRegistar;
    private javax.swing.JLabel jLabelNombreEliminar;
    private javax.swing.JLabel jLabelNombreModificar;
    private javax.swing.JLabel jLabelNombreRegistrar;
    private javax.swing.JLabel jLabelNumeroEliminar;
    private javax.swing.JLabel jLabelNumeroModificar;
    private javax.swing.JLabel jLabelNumeroRegistar;
    private javax.swing.JLabel jLabelSemestreEliminar;
    private javax.swing.JLabel jLabelSemestreModificar;
    private javax.swing.JLabel jLabelSemestreRegistrar;
    private javax.swing.JPanel jPanelCatalogo;
    private javax.swing.JPanel jPanelEliminar;
    private javax.swing.JPanel jPanelModificar;
    private javax.swing.JPanel jPanelRegistrar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextFieldApellidoMaternoModificar;
    private javax.swing.JTextField jTextFieldApellidoMaternoRegistrar;
    private javax.swing.JTextField jTextFieldApellidoPaternoModificar;
    private javax.swing.JTextField jTextFieldApellidoPaternoRegistrar;
    private javax.swing.JTextField jTextFieldCalleModificar;
    private javax.swing.JTextField jTextFieldCalleRegistrar;
    private javax.swing.JTextField jTextFieldCodigoPostalModificar;
    private javax.swing.JTextField jTextFieldCodigoPostalRegistrar;
    private javax.swing.JTextField jTextFieldColoniaModificar;
    private javax.swing.JTextField jTextFieldColoniaRegistrar;
    private javax.swing.JTextField jTextFieldMatriculaEliminar;
    private javax.swing.JTextField jTextFieldMatriculaModificar;
    private javax.swing.JTextField jTextFieldMatriculaRegistrar;
    private javax.swing.JTextField jTextFieldNombreModificar;
    private javax.swing.JTextField jTextFieldNombreRegistrar;
    private javax.swing.JTextField jTextFieldNumeroModificar;
    private javax.swing.JTextField jTextFieldNumeroRegistrar;
    private javax.swing.JTextField jTextFieldSemestreModificar;
    private javax.swing.JTextField jTextFieldSemestreRegistrar;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JLabel labelMatriculaModificar;
    // End of variables declaration//GEN-END:variables

    private final class FormListener extends mx.uacam.balam.simulacion.registrocic.util.FormListener implements FocusListener {

        public FormListener(BasicOperations bo) {
            super(bo);
        }

        @Override
        public void focusGained(FocusEvent evt) {
            if (evt.getSource() == jComboBoxEspecialidadRegistrar) {
                jComboBoxFocusGained(evt);
            } else if (evt.getSource() == jComboBoxEspecialidadModificar) {
                jComboBoxFocusGained(evt);
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }
}