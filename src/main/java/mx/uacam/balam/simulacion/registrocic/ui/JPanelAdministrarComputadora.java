/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.awt.HeadlessException;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.ComputadoraDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.EstadoPcDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.LocalizacionDAO;
import mx.uacam.balam.simulacion.registrocic.data.dao.SalaDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Computadora;
import mx.uacam.balam.simulacion.registrocic.data.entities.EstadoPc;
import mx.uacam.balam.simulacion.registrocic.data.entities.Localizacion;
import mx.uacam.balam.simulacion.registrocic.data.entities.Sala;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelComputadora;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;

/**
 * This class allow manage with a grafic user interface manage the
 * {@link mx.uacam.balam.simulacion.registrocic.data.entities.Computadora} object
 * in the database
 *
 * @author Freddy Barrera
 */
public class JPanelAdministrarComputadora extends javax.swing.JPanel implements OperationConstants, BasicOperations {

    /** Creates new form JPanelAdministrarComputadora */
    public JPanelAdministrarComputadora() {
        computadoraDAO = FACTORY.getComputadoraDAO();
        estadoPcDAO = FACTORY.getEstadoPcDAO();
        localizacionDAO = FACTORY.getLocalizacionDAO();
        salaDAO = FACTORY.getSalaDAO();
        formListener = new FormListener(this);
        initComponents();
    }

    /** This method initializes the form components */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelRegistrar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        botonRegistrar = new javax.swing.JButton();
        textSerieRegistrar = new javax.swing.JTextField();
        comboSalaRegistrar = new javax.swing.JComboBox<>();
        comboEstadoRegistrar = new javax.swing.JComboBox<>();
        comboLocalizacionRegistrar = new javax.swing.JComboBox<>();
        jPanelModificar = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        botonBuscarModificar = new javax.swing.JButton();
        textBuscarModificar = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel6 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        botonModificar = new javax.swing.JButton();
        comboSalaModificar = new javax.swing.JComboBox<>();
        comboEstadoModificar = new javax.swing.JComboBox<>();
        comboLocalizacionModificar = new javax.swing.JComboBox<>();
        labelSerieModificar = new javax.swing.JLabel();
        jPanelEliminar = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        textBuscarEliminar = new javax.swing.JTextField();
        botonBuscarEliminar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel9 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        botonEliminar = new javax.swing.JButton();
        labelSerieEliminar = new javax.swing.JLabel();
        labelSalaEliminar = new javax.swing.JLabel();
        labelEstadoEliminar = new javax.swing.JLabel();
        labelLocalizacionEliminar = new javax.swing.JLabel();
        jPanelCatalogo = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jXHeader1.setMinimumSize(new java.awt.Dimension(264, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(264, 90));
        jXHeader1.setIcon(Images.IMAGEN_COMPUTADORAS.getIcon(64, 64));
        jXHeader1.setTitle("Administrar computadora");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
        jXHeader1.setDescription("Esta opción le permite añadir, modificar, eliminar y visualizar de las computadoras.");
        add(jXHeader1, java.awt.BorderLayout.PAGE_START);

        jLabel1.setText("* Nombre PC:");

        jLabel2.setText("* Sala:");

        jLabel3.setText("* Estado:");

        jLabel4.setText("* Localizacion:");

        botonRegistrar.setText("Registrar");
        botonRegistrar.setActionCommand(COMMAND_ADD);
        botonRegistrar.addActionListener(formListener);

        comboSalaRegistrar.addFocusListener(formListener);

        comboEstadoRegistrar.addFocusListener(formListener);

        comboLocalizacionRegistrar.addFocusListener(formListener);

        javax.swing.GroupLayout jPanelRegistrarLayout = new javax.swing.GroupLayout(jPanelRegistrar);
        jPanelRegistrar.setLayout(jPanelRegistrarLayout);
        jPanelRegistrarLayout.setHorizontalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textSerieRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                    .addComponent(comboSalaRegistrar, 0, 159, Short.MAX_VALUE)
                    .addComponent(comboEstadoRegistrar, 0, 159, Short.MAX_VALUE)
                    .addComponent(comboLocalizacionRegistrar, 0, 159, Short.MAX_VALUE))
                .addGap(130, 130, 130))
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(botonRegistrar)
                .addContainerGap(308, Short.MAX_VALUE))
        );
        jPanelRegistrarLayout.setVerticalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textSerieRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(comboSalaRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(comboEstadoRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(comboLocalizacionRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonRegistrar)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Registrar", Images.IMAGEN_EDITAR.getIcon(24, 24), jPanelRegistrar);

        botonBuscarModificar.setText("Buscar");
        botonBuscarModificar.setActionCommand(COMMAND_FIND_EDIT);
        botonBuscarModificar.addActionListener(formListener);

        jLabel5.setText("Nombre PC:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBuscarModificar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSeparator1)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(textBuscarModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarModificar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel10.setText("Nombre PC:");

        jLabel11.setText("* Sala:");

        jLabel12.setText("* Estado:");

        jLabel13.setText("* Localización:");

        botonModificar.setText("Modificar");
        botonModificar.setActionCommand(COMMAND_EDIT);
        botonModificar.setEnabled(false);
        botonModificar.addActionListener(formListener);

        comboSalaModificar.addFocusListener(formListener);

        comboEstadoModificar.addFocusListener(formListener);

        comboLocalizacionModificar.addFocusListener(formListener);

        labelSerieModificar.setText("-----------");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboEstadoModificar, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboLocalizacionModificar, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelSerieModificar)
                            .addComponent(comboSalaModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(botonModificar))
                .addGap(118, 118, 118))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(labelSerieModificar))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(comboSalaModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(comboEstadoModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(comboLocalizacionModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonModificar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelModificarLayout = new javax.swing.GroupLayout(jPanelModificar);
        jPanelModificar.setLayout(jPanelModificarLayout);
        jPanelModificarLayout.setHorizontalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelModificarLayout.setVerticalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelModificarLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Modificar", Images.IMAGEN_MODIFICAR.getIcon(24, 24), jPanelModificar);

        jLabel14.setText("Nombre PC:");

        botonBuscarEliminar.setText("Buscar");
        botonBuscarEliminar.setActionCommand(COMMAND_FIND_DELETE);
        botonBuscarEliminar.addActionListener(formListener);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBuscarEliminar)
                .addContainerGap(120, Short.MAX_VALUE))
            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(textBuscarEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarEliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel19.setText("Nombre PC:");

        jLabel20.setText("Sala:");

        jLabel21.setText("Estado:");

        jLabel22.setText("Localización:");

        botonEliminar.setText("Eliminar");
        botonEliminar.setActionCommand(COMMAND_DELETE);
        botonEliminar.setEnabled(false);

        labelSerieEliminar.setText("-----------");

        labelSalaEliminar.setText("-----------");

        labelEstadoEliminar.setText("-----------");

        labelLocalizacionEliminar.setText("-----------");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelLocalizacionEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                            .addComponent(labelEstadoEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                            .addComponent(labelSalaEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                            .addComponent(labelSerieEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                        .addGap(172, 172, 172))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(botonEliminar)
                        .addContainerGap(316, Short.MAX_VALUE))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel22))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(labelSerieEliminar)
                        .addGap(18, 18, 18)
                        .addComponent(labelSalaEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelEstadoEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelLocalizacionEliminar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelEliminarLayout = new javax.swing.GroupLayout(jPanelEliminar);
        jPanelEliminar.setLayout(jPanelEliminarLayout);
        jPanelEliminarLayout.setHorizontalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelEliminarLayout.setVerticalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Eliminar", Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanelEliminar);

        jToolBar1.setFloatable(false);

        jButton2.setIcon(Images.IMAGEN_ACTUALIZAR.getIcon(20, 20));
        jButton2.setText("Actualizar");
        jButton2.setActionCommand(COMMAND_UPDATE_TABLE);
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(formListener);
        jToolBar1.add(jButton2);

        jTable1.setModel(new mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelComputadora(computadoraDAO.findAll()));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanelCatalogoLayout = new javax.swing.GroupLayout(jPanelCatalogo);
        jPanelCatalogo.setLayout(jPanelCatalogoLayout);
        jPanelCatalogoLayout.setHorizontalGroup(
            jPanelCatalogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanelCatalogoLayout.setVerticalGroup(
            jPanelCatalogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCatalogoLayout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Catálogo", Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanelCatalogo);

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * This method add a new object
     * {@link mx.uacam.balam.simulacion.registrocic.data.entities.Computadora}
     */
    @Override
    public void add() {
        if (isEmptyFields(REGISTRAR)) {
            JOptionPane.showMessageDialog(null, "Los campos marcados con un * "
                    + "son obligatorios, por ello no deben estar vacios.\n"
                    + "Complete los campos faltantes por favor",
                    "Campos obligatorios vacios", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                Sala sala = (Sala) comboSalaRegistrar.getModel().getSelectedItem();
                EstadoPc estado = (EstadoPc) comboEstadoRegistrar.getModel().getSelectedItem();
                Localizacion localizacion = (Localizacion) comboLocalizacionRegistrar.getModel().getSelectedItem();
                Computadora computadora = new Computadora(localizacion, estado, sala, textSerieRegistrar.getText());
                computadoraDAO.makePersistent(computadora);
                JOptionPane.showMessageDialog(this, "La computadora se ha registrado correctamente", "Registro completado", JOptionPane.INFORMATION_MESSAGE);
                cleanFields(REGISTRAR);
                updateTable();
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Elemnto duplicado", JOptionPane.ERROR_MESSAGE);
            } catch (HeadlessException ex) {
                JOptionPane.showMessageDialog(this, "Error al intentar registrar la computadora: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * This method edit a object
     * {@link mx.uacam.balam.simulacion.registrocic.data.entities.Computadora}
     */
    @Override
    public void edit() {
        try {
            Computadora computadora = new Computadora();
            computadora.setSerie(labelSerieModificar.getText());
            List<Computadora> listaComputadoras =
                    computadoraDAO.findByExample(computadora,
                    new String[]{"clave", "localizacion", "estadoPc", "sala"});
            computadora = listaComputadoras.get(0);
            computadora.setLocalizacion((Localizacion) comboLocalizacionModificar.getSelectedItem());
            computadora.setEstadoPc((EstadoPc) comboEstadoModificar.getSelectedItem());
            computadora.setSala((Sala) comboSalaModificar.getSelectedItem());
            computadora.setSerie(labelSerieModificar.getText());
            computadoraDAO.makePersistent(computadora);
            JOptionPane.showMessageDialog(null,
                    "La computadora se ha modificado correctamente",
                    "Modificación completada",
                    JOptionPane.INFORMATION_MESSAGE);
            cleanFields(MODIFICAR);
        } catch (DuplicateEntryException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Elemento duplicado", JOptionPane.ERROR_MESSAGE);
        } catch (HeadlessException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error inesperado", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(JPanelAdministrarComputadora.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * This method delete a object
     * {@link mx.uacam.balam.simulacion.registrocic.data.entities.Computadora}
     */
    @Override
    public void delete() {
        try {
            Computadora computadora = new Computadora();
            computadora.setSerie(labelSerieEliminar.getText());
            List<Computadora> listaComputadoras = computadoraDAO.findByExample(
                    computadora, new String[]{"clave", "localizacion",
                        "estadoPc", "sala"});
            computadora = listaComputadoras.get(0);
            computadoraDAO.makeTransient(computadora);
            JOptionPane.showMessageDialog(null, "La computadora se ha eliminado correctamente",
                    "Eliminación completada", JOptionPane.INFORMATION_MESSAGE);
            cleanFields(ELIMINAR);
        } catch (HeadlessException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error inesperado", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(JPanelAdministrarComputadora.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param field
     */
    @Override
    public void find(int field) {
        try {
            Computadora computadora = new Computadora();
            String[] excludeProperty = new String[]{
                "clave", "localizacion", "estadoPc", "sala"};

            switch (field) {
                case BUSCAR | MODIFICAR:
                    if (isEmptyFields(BUSCAR | MODIFICAR)) {
                        JOptionPane.showMessageDialog(null, "Debe ingresar un valor para realizar la busqueda", "Campo vacio", JOptionPane.WARNING_MESSAGE);
                    } else {
                        computadora.setSerie(textBuscarModificar.getText());
                        List<Computadora> listaComputadoras =
                                computadoraDAO.findByExample(computadora, excludeProperty);
                        computadora = listaComputadoras.get(0);

                        labelSerieModificar.setText(computadora.getSerie());

                        List<Sala> salas = salaDAO.findAll();
                        List<EstadoPc> estadoPcs = estadoPcDAO.findAll();
                        List<Localizacion> localizaciones = localizacionDAO.findAll();
                        comboSalaModificar.setModel(new DefaultComboBoxModel<>(salas.toArray(new Sala[salas.size()])));
                        comboEstadoModificar.setModel(new DefaultComboBoxModel<>(estadoPcs.toArray(new EstadoPc[estadoPcs.size()])));
                        comboLocalizacionModificar.setModel(new DefaultComboBoxModel<>(localizaciones.toArray(new Localizacion[localizaciones.size()])));

                        comboSalaModificar.setSelectedItem(computadora.getSala());
                        comboEstadoModificar.setSelectedItem(computadora.getEstadoPc());
                        comboLocalizacionModificar.setSelectedItem(computadora.getLocalizacion());

                        botonModificar.setEnabled(true);
                    }
                    break;
                case BUSCAR | ELIMINAR:
                    if (isEmptyFields(BUSCAR | ELIMINAR)) {
                        JOptionPane.showMessageDialog(null, "Debe ingresar un valor para realizar la busqueda", "Campo vacio", JOptionPane.WARNING_MESSAGE);
                    } else {
                        computadora.setSerie(textBuscarEliminar.getText());
                        List<Computadora> listaComputadoras =
                                computadoraDAO.findByExample(computadora, excludeProperty);
                        computadora = listaComputadoras.get(0);

                        labelSerieEliminar.setText(computadora.getSerie());
                        labelSalaEliminar.setText(computadora.getSala().getNombre());
                        labelEstadoEliminar.setText(computadora.getEstadoPc().getNombre());
                        labelLocalizacionEliminar.setText(computadora.getLocalizacion().getNombre());
                        botonEliminar.setEnabled(true);
                    }
                    break;
            }
            cleanFields(BUSCAR);
        } catch (IndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null, "La computadora que indico no se ha encontrado", "Computadora no encontrado", JOptionPane.WARNING_MESSAGE);
        } catch (HeadlessException ex) {
            JOptionPane.showMessageDialog(null, "La computadora que indico no se ha encontrado", "Computadora no encontrado", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     *
     * @param field
     */
    @Override
    public void cancelFind(int field) {
    }

    /**
     *
     * @param field
     * @return
     */
    @Override
    public boolean isEmptyFields(int field) {
        switch (field) {
            case REGISTRAR:
                return textSerieRegistrar.getText().isEmpty();
            case BUSCAR | MODIFICAR:
                return textBuscarModificar.getText().isEmpty();
            case BUSCAR | ELIMINAR:
                return textBuscarEliminar.getText().isEmpty();
            default:
                throw new IllegalArgumentException("This " + field + " no is a field.");
        }
    }

    /**
     *
     * @param fields
     */
    @Override
    public void cleanFields(int fields) {
        switch (fields) {
            case REGISTRAR:
                textSerieRegistrar.setText("");
                comboSalaRegistrar.setSelectedIndex(0);
                comboEstadoRegistrar.setSelectedIndex(0);
                comboLocalizacionRegistrar.setSelectedIndex(0);
                break;
            case BUSCAR:
                textBuscarModificar.setText("");
                textBuscarEliminar.setText("");
                break;
            case MODIFICAR:
                labelSerieModificar.setText("-----------");
                comboSalaModificar.setSelectedIndex(0);
                comboEstadoModificar.setSelectedIndex(0);
                comboLocalizacionModificar.setSelectedIndex(0);
                break;
            case ELIMINAR:
                labelSerieEliminar.setText("-----------");
                labelEstadoEliminar.setText("-----------");
                labelLocalizacionEliminar.setText("-----------");
                labelSalaEliminar.setText("-----------");
                break;
        }
    }

    /**
     *
     */
    @Override
    public void updateTable() {
        TableModelComputadora tmc = (TableModelComputadora) jTable1.getModel();
        tmc.updateModel(computadoraDAO.findAll());
    }

    /*
     *
     */
    private void comboSalaFocusGained(int field) {
        List<Sala> salas = salaDAO.findAll();
        switch (field) {
            case REGISTRAR:
                comboSalaRegistrar.setModel(new DefaultComboBoxModel<>(salas.toArray(new Sala[salas.size()])));
                break;
            case MODIFICAR:
                comboSalaModificar.setModel(new DefaultComboBoxModel<>(salas.toArray(new Sala[salas.size()])));
                break;
        }
    }

    /**
     *
     * @param field
     */
    private void comboEstadoFocusGained(int field) {
        List<EstadoPc> estadoPcs = estadoPcDAO.findAll();
        switch (field) {
            case REGISTRAR:
                comboEstadoRegistrar.setModel(new DefaultComboBoxModel<>(estadoPcs.toArray(new EstadoPc[estadoPcs.size()])));
                break;
            case MODIFICAR:
                comboEstadoModificar.setModel(new DefaultComboBoxModel<>(estadoPcs.toArray(new EstadoPc[estadoPcs.size()])));
                break;
        }
    }

    private void comboLocalizacionFocusGained(int field) {
        List<Localizacion> localizaciones = localizacionDAO.findAll();
        switch (field) {
            case REGISTRAR:
                comboLocalizacionRegistrar.setModel(new DefaultComboBoxModel<>(localizaciones.toArray(new Localizacion[localizaciones.size()])));
                break;
            case MODIFICAR:
                comboLocalizacionModificar.setModel(new DefaultComboBoxModel<>(localizaciones.toArray(new Localizacion[localizaciones.size()])));
        }

    }

    private final ComputadoraDAO computadoraDAO;
    private final EstadoPcDAO estadoPcDAO;
    private final LocalizacionDAO localizacionDAO;
    private final SalaDAO salaDAO;
    private final FormListener formListener;
    private static final long serialVersionUID = -8242062387922934428L;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscarEliminar;
    private javax.swing.JButton botonBuscarModificar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonRegistrar;
    private javax.swing.JComboBox<EstadoPc> comboEstadoModificar;
    private javax.swing.JComboBox<EstadoPc> comboEstadoRegistrar;
    private javax.swing.JComboBox<Localizacion> comboLocalizacionModificar;
    private javax.swing.JComboBox<Localizacion> comboLocalizacionRegistrar;
    private javax.swing.JComboBox<Sala> comboSalaModificar;
    private javax.swing.JComboBox<Sala> comboSalaRegistrar;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelCatalogo;
    private javax.swing.JPanel jPanelEliminar;
    private javax.swing.JPanel jPanelModificar;
    private javax.swing.JPanel jPanelRegistrar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JLabel labelEstadoEliminar;
    private javax.swing.JLabel labelLocalizacionEliminar;
    private javax.swing.JLabel labelSalaEliminar;
    private javax.swing.JLabel labelSerieEliminar;
    private javax.swing.JLabel labelSerieModificar;
    private javax.swing.JTextField textBuscarEliminar;
    private javax.swing.JTextField textBuscarModificar;
    private javax.swing.JTextField textSerieRegistrar;
    // End of variables declaration//GEN-END:variables

    private final class FormListener extends mx.uacam.balam.simulacion.registrocic.util.FormListener implements FocusListener {

        public FormListener(BasicOperations bo) {
            super(bo);
        }

        @Override
        public void focusGained(FocusEvent evt) {
            if (evt.getSource() == comboSalaRegistrar) {
                comboSalaFocusGained(REGISTRAR);
            } else if (evt.getSource() == comboEstadoRegistrar) {
                comboEstadoFocusGained(REGISTRAR);
            } else if (evt.getSource() == comboLocalizacionRegistrar) {
                comboLocalizacionFocusGained(REGISTRAR);
            } else if (evt.getSource() == comboSalaModificar) {
                comboSalaFocusGained(MODIFICAR);
            } else if (evt.getSource() == comboEstadoModificar) {
                comboEstadoFocusGained(MODIFICAR);
            } else if (evt.getSource() == comboLocalizacionModificar) {
                comboLocalizacionFocusGained(MODIFICAR);
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
        }
    }
}