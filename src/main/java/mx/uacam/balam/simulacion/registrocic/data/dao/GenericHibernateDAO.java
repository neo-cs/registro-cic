/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

/**
 *
 * @param <T> the entity.
 * @param <K> the type of the primary key.
 * @author Freddy Barrera
 */
// TODO: Migrate to JPA
public abstract class GenericHibernateDAO<T, K extends Serializable> implements GenericDAO<T, K> {

    private final Class<T> persistentClass;
    private Session session;

    @SuppressWarnings("unchecked")
    public GenericHibernateDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void setSession(Session s) {
        this.session = s;
    }

    protected Session getSession() {
        if (session == null) {
            throw new IllegalStateException("Session has not been set on DAO before usage");
        }
        return session;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findById(K id, boolean lock) throws EntryNotFoundException {
        T entity;
        try {
            if (lock) {
                entity = getSession().load(getPersistentClass(), id, LockMode.PESSIMISTIC_WRITE);
            } else {
                entity = getSession().load(getPersistentClass(), id);
            }
        } catch (ObjectNotFoundException ex) {
            throw new EntryNotFoundException(ex.getMessage());
        }

        return entity;
    }

    @Override
    public List<T> findAll() {
        return findByCriteria();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> findByExample(T exampleInstance, String[] excludeProperty) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Example example = Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    /**
     * This method save or update a <code>entity</code>
     *
     * @param entity
     * @return
     * @throws mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException
     */
    @Override
    public T makePersistent(T entity) throws DuplicateEntryException {
        Transaction transaction = getSession().beginTransaction();
        try {
            getSession().saveOrUpdate(entity);
            transaction.commit();
        } catch (org.hibernate.NonUniqueObjectException ex) {
            transaction.rollback();
            throw new DuplicateEntryException(ex.getMessage());
        } catch (org.hibernate.exception.ConstraintViolationException ex){
            transaction.rollback();
            throw new DuplicateEntryException(ex.getMessage());
        }

        return entity;
    }

    /**
     * This method delete a <code>entity</code>
     *
     * @param entity to delete
     */
    @Override
    public void makeTransient(T entity) {
        Transaction transaction = getSession().beginTransaction();
        getSession().delete(entity);
        transaction.commit();
    }

    public void flush() {
        getSession().flush();
    }

    public void clear() {
        getSession().clear();
    }

    /**
     * Use this inside subclasses as a convenience method.
     * @param criterion
     * @return
     */
    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }
}