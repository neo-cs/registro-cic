/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

/**
 *
 * @author Freddy Barrera
 */
public interface ActionCommand {
    String COMMAND_ADD = "COMMAND_ADD";
    String COMMAND_EDIT = "COMMAND_EDIT";
    String COMMAND_FIND_EDIT = "COMMAND_FIND_EDIT";
    String COMMAND_FIND_DELETE = "COMMAND_FIND_DELETE";
    String COMMAND_CANCEL_FIND_EDIT = "COMMAND_CANCEL_FIND_EDIT";
    String COMMAND_CANCEL_FIND_DELETE = "COMMAND_CANCEL_FIND_DELETE";
    String COMMAND_DELETE = "COMMAND_DELETE";
    String COMMAND_UPDATE_TABLE = "COMMAND_UPDATE_TABLE";
}
