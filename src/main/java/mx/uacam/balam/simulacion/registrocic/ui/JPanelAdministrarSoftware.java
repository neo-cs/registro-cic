/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.SoftwareDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Software;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelSoftware;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.FormListener;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

/**
 *
 * @author six
 */
public class JPanelAdministrarSoftware extends javax.swing.JPanel
         implements OperationConstants, BasicOperations {
    public JPanelAdministrarSoftware() {
        softwareDAO = FACTORY.getSoftwareDAO();
        formListener = new FormListener(this);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        textRegistrarNombreSoftware = new javax.swing.JTextField();
        botonRegistrarSoftware = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        labelModificarIdSoftware = new javax.swing.JLabel();
        textModificarNombreSoftware = new javax.swing.JTextField();
        botonModificarSoftware = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        botonBuscarModificarSoftware = new javax.swing.JButton();
        textBuscarModificarSoftware = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButtonCancelarModificar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        labelEliminarIdSoftware = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelEliminarNombreSoftware = new javax.swing.JLabel();
        botonEliminarSoftware = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        textBuscarEliminarSoftware = new javax.swing.JTextField();
        botonBuscarEliminarSoftware = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jButtonCancelarEliminar = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        jButtonActualizar = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jXHeader1.setIcon(Images.IMAGEN_SOFTWARE.getIcon(64, 64));
        jXHeader1.setTitle("Administrar software");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
        jXHeader1.setDescription("Esta opción le permite "
            + "añadir, modificar, eliminar y visualizar el software.");
        jXHeader1.setMinimumSize(new java.awt.Dimension(46, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(51, 90));
        add(jXHeader1, java.awt.BorderLayout.PAGE_START);

        jLabel2.setText("Nombre:");

        botonRegistrarSoftware.setText("Registrar");
        botonRegistrarSoftware.setActionCommand(COMMAND_ADD);
        botonRegistrarSoftware.addActionListener(formListener);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textRegistrarNombreSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonRegistrarSoftware))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textRegistrarNombreSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonRegistrarSoftware)
                .addContainerGap(110, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Registar", Images.IMAGEN_EDITAR.getIcon(24, 24), jPanel3);

        jLabel3.setText("Clave:");

        jLabel4.setText("Nombre:");

        labelModificarIdSoftware.setText("----------");

        botonModificarSoftware.setText("Modificar");
        botonModificarSoftware.setActionCommand(COMMAND_EDIT);
        botonModificarSoftware.setEnabled(false);
        botonModificarSoftware.addActionListener(formListener);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textModificarNombreSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelModificarIdSoftware, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                        .addContainerGap(155, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(botonModificarSoftware)
                        .addContainerGap(310, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(labelModificarIdSoftware)
                        .addGap(18, 18, 18)
                        .addComponent(textModificarNombreSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonModificarSoftware)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        botonBuscarModificarSoftware.setText("Buscar");
        botonBuscarModificarSoftware.setActionCommand(COMMAND_FIND_EDIT);
        botonBuscarModificarSoftware.addActionListener(formListener);

        jLabel6.setText("Clave:");

        jButtonCancelarModificar.setText("Cancelar");
        jButtonCancelarModificar.setActionCommand(COMMAND_CANCEL_FIND_EDIT);
        jButtonCancelarModificar.addActionListener(formListener);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarModificarSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonBuscarModificarSoftware)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelarModificar)
                .addContainerGap(95, Short.MAX_VALUE))
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textBuscarModificarSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarModificarSoftware)
                    .addComponent(jLabel6)
                    .addComponent(jButtonCancelarModificar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Modificar", Images.IMAGEN_MODIFICAR.getIcon(24, 24), jPanel2);

        jLabel5.setText("Clave:");

        labelEliminarIdSoftware.setText("----------");

        jLabel7.setText("Nombre");

        labelEliminarNombreSoftware.setText("----------");

        botonEliminarSoftware.setText("Eliminar");
        botonEliminarSoftware.setActionCommand(COMMAND_DELETE);
        botonEliminarSoftware.setEnabled(false);
        botonEliminarSoftware.addActionListener(formListener);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelEliminarIdSoftware, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                            .addComponent(labelEliminarNombreSoftware, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                        .addContainerGap(212, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(botonEliminarSoftware)
                        .addContainerGap(316, Short.MAX_VALUE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(labelEliminarIdSoftware)
                        .addGap(18, 18, 18)
                        .addComponent(labelEliminarNombreSoftware)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEliminarSoftware)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jLabel8.setText("Clave:");

        botonBuscarEliminarSoftware.setText("Buscar");
        botonBuscarEliminarSoftware.setActionCommand(COMMAND_FIND_DELETE);
        botonBuscarEliminarSoftware.addActionListener(formListener);

        jButtonCancelarEliminar.setText("Cancelar");
        jButtonCancelarEliminar.setActionCommand(COMMAND_CANCEL_FIND_DELETE);
        jButtonCancelarEliminar.addActionListener(formListener);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarEliminarSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonBuscarEliminarSoftware)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonCancelarEliminar)
                .addContainerGap(111, Short.MAX_VALUE))
            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textBuscarEliminarSoftware, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarEliminarSoftware)
                    .addComponent(jLabel8)
                    .addComponent(jButtonCancelarEliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Eliminar", Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanel1);

        jTable1.setModel(new mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelSoftware(softwareDAO.findAll()));
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        jScrollPane1.setViewportView(jTable1);

        jToolBar1.setFloatable(false);

        jButtonActualizar.setIcon(Images.IMAGEN_ACTUALIZAR.getIcon(20, 20));
        jButtonActualizar.setText("Actualizar");
        jButtonActualizar.setToolTipText("Actualizar");
        jButtonActualizar.setActionCommand(COMMAND_UPDATE_TABLE);
        jButtonActualizar.setFocusable(false);
        jButtonActualizar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonActualizar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonActualizar.addActionListener(formListener);
        jToolBar1.add(jButtonActualizar);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Catálogo", Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanel8);

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Registrar");
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void add() {
        if (!isEmptyFields(REGISTRAR)) {
            try {
                Software software = new Software(textRegistrarNombreSoftware.getText());
                softwareDAO.makePersistent(software);
                cleanFields(REGISTRAR);
                updateTable();
                JOptionPane.showMessageDialog(null,
                        bundle.getString("dialog.saveCorrectly.message"),
                        bundle.getString("dialog.saveCorrectly.title"),
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(null,
                        bundle.getString("dialog.duplicateEntry.message"),
                        bundle.getString("dialog.duplicateEntry.title"),
                        JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                ErrorInfo ei = new ErrorInfo(
                        bundle.getString("dialog.unexpectedError.title"),
                        bundle.getString("dialog.unexpectedError.message")
                        + ex.getMessage(), null, null, ex, null, null);
                JXErrorPane.showDialog(null, ei);
                Logger.getLogger(JPanelAdministrarMaestro.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        } else {
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.emptyRequestFields.message"),
                    bundle.getString("dialog.emptyRequestFields.title"),
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    @Override
    public void edit() {
        try {
            Short id = Short.valueOf(labelModificarIdSoftware.getText());
            Software software = softwareDAO.findById(id, true);
            software.setNombre(textModificarNombreSoftware.getText());
            softwareDAO.makePersistent(software);
            cleanFields(MODIFICAR);
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.editCorrectly.message"),
                    bundle.getString("dialog.editCorrectly.title"),
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.entityNotFound.message"),
                    bundle.getString("dialog.entityNotFound.title"),
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            ErrorInfo ei = new ErrorInfo(
                    bundle.getString("dialog.unexpectedError.title"),
                    bundle.getString("dialog.unexpectedError.message")
                    + ex.getMessage(), null, null, ex, null, null);
            JXErrorPane.showDialog(null, ei);
            Logger.getLogger(JPanelAdministrarMaestro.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void delete() {
        try {
            Short id = Short.valueOf(labelEliminarIdSoftware.getText());
            Software software = softwareDAO.findById(id, true);
            softwareDAO.makeTransient(software);
            cleanFields(ELIMINAR);
            updateTable();
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.deleteCorrectly.message"),
                    bundle.getString("dialog.deleteCorrectly.title"),
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null,
                    bundle.getString("dialog.entityNotFound.message"),
                    bundle.getString("dialog.entityNotFound.title"),
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            ErrorInfo ei = new ErrorInfo(
                    bundle.getString("dialog.unexpectedError.title"),
                    bundle.getString("dialog.unexpectedError.message")
                    + ex.getMessage(), null, null, ex, null, null);
            JXErrorPane.showDialog(null, ei);
            Logger.getLogger(JPanelAdministrarMaestro.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void find(int field) {
        switch (field) {
            case BUSCAR | MODIFICAR:
                if (!isEmptyFields(field)) {
                    try {
                        Short id = Short.valueOf(textBuscarModificarSoftware.getText());
                        Software software = softwareDAO.findById(id, true);
                        labelModificarIdSoftware.setText(software.getIdSoftware().toString());
                        textModificarNombreSoftware.setText(software.getNombre());
                        cleanFields(BUSCAR | MODIFICAR);
                    } catch (EntryNotFoundException ex) {
                        JOptionPane.showMessageDialog(null,
                                bundle.getString("dialog.entityNotFound.message"),
                                bundle.getString("dialog.entityNotFound.title"),
                                JOptionPane.ERROR_MESSAGE);
                    } catch (Exception ex) {
                        ErrorInfo ei = new ErrorInfo(
                                bundle.getString("dialog.unexpectedError.title"),
                                bundle.getString("dialog.unexpectedError.message")
                                + ex.getMessage(), null, null, ex, null, null);
                        JXErrorPane.showDialog(null, ei);
                        Logger.getLogger(JPanelAdministrarMaestro.class.getName()).log(Level.SEVERE,
                                ex.getMessage(), ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            bundle.getString("dialog.emptyRequestFields.message"), bundle.getString("dialog.emptyRequestFields.title"), JOptionPane.WARNING_MESSAGE);
                }
                break;
            case BUSCAR | ELIMINAR:
                if (!isEmptyFields(field)) {
                    try {
                        Short id = Short.valueOf(textBuscarEliminarSoftware.getText());
                        Software software = softwareDAO.findById(id, true);
                        labelEliminarIdSoftware.setText(software.getIdSoftware().toString());
                        labelEliminarNombreSoftware.setText(software.getNombre());
                        cleanFields(BUSCAR | ELIMINAR);
                    } catch (EntryNotFoundException ex) {
                        JOptionPane.showMessageDialog(null,
                                bundle.getString("dialog.entityNotFound.message"),
                                bundle.getString("dialog.entityNotFound.title"),
                                JOptionPane.ERROR_MESSAGE);
                    } catch (Exception ex) {
                        ErrorInfo ei = new ErrorInfo(
                                bundle.getString("dialog.unexpectedError.title"),
                                bundle.getString("dialog.unexpectedError.message")
                                + ex.getMessage(), null, null, ex, null, null);
                        JXErrorPane.showDialog(null, ei);
                        Logger.getLogger(JPanelAdministrarMaestro.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            bundle.getString("dialog.emptyRequestFields.message"), bundle.getString("dialog.emptyRequestFields.title"), JOptionPane.WARNING_MESSAGE);
                }
                break;
        }
    }

    @Override
    public void cancelFind(int field) {
        switch (field) {
            case MODIFICAR:
                cleanFields(BUSCAR | MODIFICAR);
                cleanFields(field);
                break;
            case ELIMINAR:
                cleanFields(BUSCAR | ELIMINAR);
                cleanFields(field);
                break;
        }
    }

    @Override
    public boolean isEmptyFields(int field) {
        switch (field) {
            case REGISTRAR:
                return textRegistrarNombreSoftware.getText().isEmpty();
            case BUSCAR | MODIFICAR:
                return textBuscarModificarSoftware.getText().isEmpty();
            case BUSCAR | ELIMINAR:
                return textBuscarEliminarSoftware.getText().isEmpty();
            default:
                throw new IllegalArgumentException("Input:" + field);
        }
    }

    @Override
    public void cleanFields(int fields) {
        switch (fields) {
            case REGISTRAR:
                textRegistrarNombreSoftware.setText("");
                break;
            case BUSCAR | MODIFICAR:
                textBuscarModificarSoftware.setText("");
                botonModificarSoftware.setEnabled(true);
                break;
            case BUSCAR | ELIMINAR:
                textBuscarEliminarSoftware.setText("");
                botonEliminarSoftware.setEnabled(true);
                break;
            case MODIFICAR:
                labelModificarIdSoftware.setText(bundle.getString("label.line"));
                textModificarNombreSoftware.setText("");
                botonModificarSoftware.setEnabled(false);
                break;
            case ELIMINAR:
                labelEliminarIdSoftware.setText(bundle.getString("label.line"));
                labelEliminarNombreSoftware.setText(bundle.getString("label.line"));
                botonEliminarSoftware.setEnabled(false);
                break;
        }
    }

    @Override
    public void updateTable() {
        TableModelSoftware tms = (TableModelSoftware) jTable1.getModel();
        tms.updateModel(softwareDAO.findAll());
    }

    private static final long serialVersionUID = 3910175826284786398L;
    private SoftwareDAO softwareDAO;
    private FormListener formListener;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscarEliminarSoftware;
    private javax.swing.JButton botonBuscarModificarSoftware;
    private javax.swing.JButton botonEliminarSoftware;
    private javax.swing.JButton botonModificarSoftware;
    private javax.swing.JButton botonRegistrarSoftware;
    private javax.swing.JButton jButtonActualizar;
    private javax.swing.JButton jButtonCancelarEliminar;
    private javax.swing.JButton jButtonCancelarModificar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JLabel labelEliminarIdSoftware;
    private javax.swing.JLabel labelEliminarNombreSoftware;
    private javax.swing.JLabel labelModificarIdSoftware;
    private javax.swing.JTextField textBuscarEliminarSoftware;
    private javax.swing.JTextField textBuscarModificarSoftware;
    private javax.swing.JTextField textModificarNombreSoftware;
    private javax.swing.JTextField textRegistrarNombreSoftware;
    // End of variables declaration//GEN-END:variables
}