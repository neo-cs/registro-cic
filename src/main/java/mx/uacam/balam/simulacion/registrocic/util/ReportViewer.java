/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Freddy Barrera
 */
public class ReportViewer {
    private static final Logger LOGGER = LogManager.getLogger(ReportViewer.class);

//TODO  Que este metodo no cierre la aplicacion en lugar debe lanzar excepciones

    public void mostrarReporte() {
        Connection conn = null;
        // Cargamos el driver JDBC
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("MySQL JDBC Driver not found.");
//            System.exit(1);
        }

        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3336/cic", "cic_user", "admin");
            conn.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("Error de conexión: " + e.getMessage());
//            System.exit(4);
        }

        try {
            URL url = ReportViewer.class.getResource("/mx/uacam/fdi/cic/recursos/reportes/report_computadora.jasper");
            String fileName = url.toURI().getPath();
            File userHome = new File(System.getProperty("user.home", "."));
            File path = new File(userHome, ".cic_adm");
            if (!path.exists()) {
                path.mkdir();
            }

            Calendar fecha = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");

            String outFileName = path.getPath() + System.getProperty("file.separator")
                    + "reporte_computadora_" + dateFormat.format(fecha.getTime()) + ".pdf";

            HashMap hm = new HashMap();
            try {
                // Fill the report using an empty data source
                JasperPrint print = JasperFillManager.fillReport(fileName, hm, conn);

                // Create a PDF exporter
                JRExporter exporter = new JRPdfExporter();

                // Configure the exporter (set output file name and print object)
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outFileName);
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);

                // Export the PDF file
                exporter.exportReport();
                //Para visualizar el pdf directamente desde java
                JasperViewer.viewReport(print, false);
            } catch (JRException e) {
                e.printStackTrace();
//                System.exit(1);
            } catch (Exception e) {
                e.printStackTrace();
//                System.exit(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /*
             * Cleanup antes de salir
             */
            try {
                if (conn != null) {
                    conn.rollback();
                    System.out.println("ROLLBACK EJECUTADO");
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
