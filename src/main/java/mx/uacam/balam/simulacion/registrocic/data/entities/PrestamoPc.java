/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.entities;
// Generated 29-jun-2010 12:20:47 by Hibernate Tools 3.2.1.GA

import java.io.Serializable;
import java.util.Date;

/**
 * PrestamoPc generated by hbm2java
 */
public class PrestamoPc implements Serializable {

    private static final long serialVersionUID = -1485936126295759303L;
    private PrestamoPcId id;
    private Computadora computadora;
    private Software software;
    private Alumno alumno;
    private Date horaFin;

    public PrestamoPc() {
    }

    public PrestamoPc(PrestamoPcId id, Computadora computadora, Software software, Alumno alumno) {
        this.id = id;
        this.computadora = computadora;
        this.software = software;
        this.alumno = alumno;
    }

    public PrestamoPc(PrestamoPcId id, Computadora computadora, Software software, Alumno alumno, Date horaFin) {
        this.id = id;
        this.computadora = computadora;
        this.software = software;
        this.alumno = alumno;
        this.horaFin = horaFin;
    }

    public PrestamoPcId getId() {
        return this.id;
    }

    public void setId(PrestamoPcId id) {
        this.id = id;
    }

    public Computadora getComputadora() {
        return this.computadora;
    }

    public void setComputadora(Computadora computadora) {
        this.computadora = computadora;
    }

    public Software getSoftware() {
        return this.software;
    }

    public void setSoftware(Software software) {
        this.software = software;
    }

    public Alumno getAlumno() {
        return this.alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Date getHoraFin() {
        return this.horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    @Override
    public String toString() {
        return "PrestamoPc{" + "id=" + id + " computadora=" + computadora + " software=" + software + " alumno=" + alumno + " horaFin=" + horaFin + '}';
    }
}