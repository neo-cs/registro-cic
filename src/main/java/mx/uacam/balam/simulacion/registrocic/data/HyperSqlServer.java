/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * This class manage the database engine.
 *
 * @author Freddy Barrera
 */
public class HyperSqlServer {

    private static final Logger LOGGER = LogManager.getFormatterLogger(HyperSqlServer.class);
    private static final String DRIVER;
    private static final String DATABASE_NAME;
    private static final String URL;
    private static final String USERNAME;
    private static final String PASSWORD;
    private static HyperSqlServer databaseServer;
    private static Connection connection;

    private static final String EXIST_SCHEMA
            = "SELECT count(*) AS total"
            + " FROM   INFORMATION_SCHEMA.TABLES"
            + " WHERE TABLE_SCHEMA = 'CIC';";
    private static final String CREATE_SCHEMA = "CREATE SCHEMA cic;";
    private static final String SET_SCHEMA = "SET SCHEMA cic;";
    private static final String SHUTDOWN_SERVER = "SHUTDOWN";
    
    private static final String MESSAGE_RIGHT = "%s :: Correcto";
    private static final String MESSAGE_ERROR = "%s :: Error";
    
    static {
        try (InputStream is = HyperSqlServer.class.getClassLoader()
                .getResourceAsStream("mx/uacam/balam/simulacion/registrocic/data"
                        + "/configuration.properties")) {
            Properties properties = new Properties();
            properties.load(is);

            DRIVER = properties.getProperty("cic.db.driver");
            DATABASE_NAME = properties.getProperty("cic.db.name");
            String userHome = System.getProperty("user.home", ".");
            String separator =  System.getProperty("file.separator");
            URL = MessageFormat.format("jdbc:hsqldb:file:{0}{4}{1}{4}{2}{4}{3}",
                    userHome,                              // 0
                    properties.getProperty("cic.app.dir"), // 1
                    properties.getProperty("cic.db.dir"),  // 2
                    DATABASE_NAME,                         // 3
                    separator);                            // 4
            USERNAME = properties.getProperty("cic.db.username");
            PASSWORD = properties.getProperty("cic.db.password");
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    /**
     * Initialize a new instance.
     */
    private HyperSqlServer() {
    }

    private static boolean existSchema() {
        boolean existe = false;
        try (PreparedStatement prepareStatement = connection.prepareStatement(EXIST_SCHEMA)) {
            ResultSet rs = prepareStatement.executeQuery();
            int total = 0;
            while (rs.next()) {
                total = rs.getInt(1);
            }
            existe = total > 0;
        } catch (SQLException ex) {
            LOGGER.log(Level.FATAL, ex.getMessage(), ex);
        }
        return existe;
    }
    
    private static void createSchema() {
        if (executeScript(connection, CREATE_SCHEMA)) {
            LOGGER.info(MESSAGE_RIGHT, "Creación del esquema");
        } else {
            LOGGER.error(MESSAGE_ERROR, "Creación del esquema");
        }
    }

    private static void createTables() {
        for (SqlScript script : SqlScript.values()) {
            if(SqlScript.CREATE_TABLE == script.getTypeScript() || SqlScript.ALTER_TABLE == script.getTypeScript()) {
                if (executeScript(connection, script.getScript())) {
                    LOGGER.info(MESSAGE_RIGHT, script.name());
                } else {
                    LOGGER.error(MESSAGE_ERROR, script.name());
                }
            }
        }
    }
    
    private static void populateTables() {
        try (Statement statement = connection.createStatement()) {
            if (statement.execute(SqlScript.EXIST_ESTADO_PC_1.getScript())) {
                ResultSet rs = statement.getResultSet();
                boolean exist = false;
                while (rs.next()) {
                    exist = rs.getBoolean("exist");
                }
                
                if (!exist) {
                    statement.executeUpdate(SqlScript.INSERT_ESTADO_PC_1.getScript());
                    LOGGER.info(MESSAGE_RIGHT, SqlScript.INSERT_ESTADO_PC_1.name());
                }
            }
            
            if (statement.execute(SqlScript.EXIST_ESTADO_PC_2.getScript())) {
                ResultSet rs = statement.getResultSet();
                boolean exist = false;
                while (rs.next()) {
                    exist = rs.getBoolean("exist");
                }
                
                if (!exist) {
                    statement.executeUpdate(SqlScript.INSERT_ESTADO_PC_2.getScript());
                    LOGGER.info(MESSAGE_RIGHT, SqlScript.INSERT_ESTADO_PC_2.name());
                }
            }
            
            if (statement.execute(SqlScript.HAS_LOCALIZACION.getScript())) {
                ResultSet rs = statement.getResultSet();
                boolean hasLocalizacion = false;
                while (rs.next()) {
                    hasLocalizacion = rs.getBoolean("tieneLocalizacion");
                }
                
                if (!hasLocalizacion) {
                    statement.executeUpdate(SqlScript.INSERT_LOCALIZACION.getScript());
                    LOGGER.info(MESSAGE_RIGHT, SqlScript.INSERT_LOCALIZACION.name());
                }
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.FATAL, ex.getMessage(), ex);
        }
    }

    private static boolean executeScript(Connection databaseConnection, String sqlScript) {
        try (PreparedStatement prepareStatement = databaseConnection.prepareStatement(sqlScript)) {
            prepareStatement.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.log(Level.FATAL, ex.getMessage(), ex);
            return false;
        }
    }

    /**
     * Start the database server.
     * @return a instance of the server.
     */
    public static HyperSqlServer start() {
        try {
            if (databaseServer == null) {
                databaseServer = new HyperSqlServer();
                Class.forName(DRIVER);
                connection = DriverManager.getConnection(URL);
            
                if (!existSchema()) {
                    createSchema();
                }
                
                if (executeScript(connection, SET_SCHEMA)) {
                    LOGGER.info(MESSAGE_RIGHT, "Usar esquema CIC");
                } else {
                    LOGGER.error(MESSAGE_ERROR, "Usar esquema CIC");
                }
                
                createTables();
                populateTables();
            }
        } catch (SQLException | ClassNotFoundException ex) {
            LOGGER.log(Level.FATAL, ex.getMessage(), ex);
        }
        return databaseServer;
    }

    /**
     * Shutdown the database server.
     */
    public static void shutdown() {
        try (Statement aStatement = connection.createStatement()) {
            aStatement.executeUpdate(SHUTDOWN_SERVER);
            connection.close();
        } catch (SQLException ex) {
            LOGGER.log(Level.FATAL, ex.getMessage(), ex);
        }
    }

    /**
     * Get a connection to the database.
     * 
     * @return the connection.
     */
    public Connection getConnection() {
        return connection;
    }
}
