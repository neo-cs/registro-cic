/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.EspecialidadDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.Especialidad;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelEspecialidad;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.FormListener;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;

/**
 *
 * @author six
 */
public class JPanelAdministrarEspecialidad extends javax.swing.JPanel implements OperationConstants, BasicOperations {

    /** Creates new form PanelAdministrarEspecialidad */
    public JPanelAdministrarEspecialidad() {
        especialidadDAO = FACTORY.getEspecialidadDAO();
        formListener = new FormListener(this);
        initComponents();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelRegistrar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textRegistrarClaveEspecialidad = new javax.swing.JTextField();
        textRegistrarNombreEspecialidad = new javax.swing.JTextField();
        botonRegistrarEspecialidad = new javax.swing.JButton();
        jPanelModificar = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        textBuscarModificarEspecialidad = new javax.swing.JTextField();
        botonBuscarModificarEspecialidad = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textModificarNombreEspecialidad = new javax.swing.JTextField();
        labelModificarClaveEspecialidad = new javax.swing.JLabel();
        botonModificarEspecialidad = new javax.swing.JButton();
        jPanelEliminar = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        labelEliminarClaveEspecialidad = new javax.swing.JLabel();
        labelEliminarNombreEspecialidad = new javax.swing.JLabel();
        botonEliminarEspecialidad = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        botonBuscarEliminarEspecialidad = new javax.swing.JButton();
        textBuscarEliminarEspecialidad = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelCatalogo = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jXHeader1.setMinimumSize(new java.awt.Dimension(264, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(51, 90));
        jXHeader1.setIcon(Images.IMAGEN_USUARIO.getIcon());
        jXHeader1.setTitle("Administrar especialidad");
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
        jXHeader1.setDescription("Esta opción le permite añadir, modificar, eliminar y visualizar las especialidades.");
        add(jXHeader1, java.awt.BorderLayout.PAGE_START);

        jLabel1.setText("*Clave:");

        jLabel2.setText("*Nombre:");

        botonRegistrarEspecialidad.setText("Registrar");
        botonRegistrarEspecialidad.setActionCommand(COMMAND_ADD);
        botonRegistrarEspecialidad.addActionListener(formListener);

        javax.swing.GroupLayout jPanelRegistrarLayout = new javax.swing.GroupLayout(jPanelRegistrar);
        jPanelRegistrar.setLayout(jPanelRegistrarLayout);
        jPanelRegistrarLayout.setHorizontalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textRegistrarNombreEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                            .addComponent(textRegistrarClaveEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)))
                    .addComponent(botonRegistrarEspecialidad))
                .addContainerGap(183, Short.MAX_VALUE))
        );
        jPanelRegistrarLayout.setVerticalGroup(
            jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegistrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(textRegistrarClaveEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelRegistrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textRegistrarNombreEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonRegistrarEspecialidad)
                .addContainerGap(79, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Registrar", Images.IMAGEN_EDITAR.getIcon(24, 24), jPanelRegistrar);

        jLabel5.setText("Clave");

        botonBuscarModificarEspecialidad.setText("Buscar");
        botonBuscarModificarEspecialidad.setActionCommand(COMMAND_FIND_EDIT);
        botonBuscarModificarEspecialidad.setFocusable(false);
        botonBuscarModificarEspecialidad.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonBuscarModificarEspecialidad.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        botonBuscarModificarEspecialidad.addActionListener(formListener);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(textBuscarModificarEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBuscarModificarEspecialidad)
                .addContainerGap(57, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(textBuscarModificarEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonBuscarModificarEspecialidad))
                .addGap(7, 7, 7)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("Clave");

        jLabel4.setText("*Nombre");
        jLabel4.setAlignmentY(0.6F);

        textModificarNombreEspecialidad.setEditable(false);

        labelModificarClaveEspecialidad.setText("-----------");

        botonModificarEspecialidad.setText("Modificar");
        botonModificarEspecialidad.setActionCommand(COMMAND_EDIT);
        botonModificarEspecialidad.setEnabled(false);
        botonModificarEspecialidad.addActionListener(formListener);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelModificarClaveEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textModificarNombreEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(botonModificarEspecialidad)))
                .addGap(179, 179, 179))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(labelModificarClaveEspecialidad))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textModificarNombreEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addComponent(botonModificarEspecialidad)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelModificarLayout = new javax.swing.GroupLayout(jPanelModificar);
        jPanelModificar.setLayout(jPanelModificarLayout);
        jPanelModificarLayout.setHorizontalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanelModificarLayout.setVerticalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelModificarLayout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Modificar", Images.IMAGEN_MODIFICAR.getIcon(24, 24), jPanelModificar);

        jLabel6.setText("Clave:");

        jLabel7.setText("Nombre:");

        labelEliminarClaveEspecialidad.setText("-----------");

        labelEliminarNombreEspecialidad.setText("-----------");

        botonEliminarEspecialidad.setText("Eliminar");
        botonEliminarEspecialidad.setActionCommand(COMMAND_DELETE);
        botonEliminarEspecialidad.setEnabled(false);
        botonEliminarEspecialidad.addActionListener(formListener);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(labelEliminarNombreEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelEliminarClaveEspecialidad, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)))
                    .addComponent(botonEliminarEspecialidad))
                .addContainerGap(211, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(labelEliminarClaveEspecialidad)
                        .addGap(18, 18, 18)
                        .addComponent(labelEliminarNombreEspecialidad))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEliminarEspecialidad)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        botonBuscarEliminarEspecialidad.setText("Buscar");
        botonBuscarEliminarEspecialidad.setActionCommand(COMMAND_FIND_DELETE);
        botonBuscarEliminarEspecialidad.addActionListener(formListener);

        jLabel8.setText("Clave");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarEliminarEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarEliminarEspecialidad)
                .addContainerGap(145, Short.MAX_VALUE))
            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textBuscarEliminarEspecialidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarEliminarEspecialidad)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelEliminarLayout = new javax.swing.GroupLayout(jPanelEliminar);
        jPanelEliminar.setLayout(jPanelEliminarLayout);
        jPanelEliminarLayout.setHorizontalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelEliminarLayout.setVerticalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEliminarLayout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Eliminar", Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanelEliminar);

        jToolBar1.setFloatable(false);

        jButton1.setIcon(Images.IMAGEN_ACTUALIZAR.getIcon(20,20));
        jButton1.setText("Actualizar");
        jButton1.setToolTipText("Actualizar");
        jButton1.setActionCommand(COMMAND_UPDATE_TABLE);
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(formListener);
        jToolBar1.add(jButton1);

        jTable1.setModel(new mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelEspecialidad(especialidadDAO.findAll()));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanelCatalogoLayout = new javax.swing.GroupLayout(jPanelCatalogo);
        jPanelCatalogo.setLayout(jPanelCatalogoLayout);
        jPanelCatalogoLayout.setHorizontalGroup(
            jPanelCatalogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanelCatalogoLayout.setVerticalGroup(
            jPanelCatalogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCatalogoLayout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Catálogo", Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanelCatalogo);

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void add() {
        if (isEmptyFields(REGISTRAR)) {
            JOptionPane.showMessageDialog(null, "Los campos con * son obligatorios verifiquelos", "Campos obligatorios vacios", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                Especialidad especialidad = new Especialidad(textRegistrarClaveEspecialidad.getText(), textRegistrarNombreEspecialidad.getText());
                especialidadDAO.makePersistent(especialidad);
                JOptionPane.showMessageDialog(null, "La especialidad se ha registrado", "Registro completado", JOptionPane.INFORMATION_MESSAGE);
                cleanFields(REGISTRAR);
                updateTable();
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Elemento duplicado", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Error al intentar registrar la especialidad " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void edit() {
        try {
            if (isEmptyFields(MODIFICAR)) {
                JOptionPane.showMessageDialog(null, "El campo Nombre esta vacio", "Campo obligatorio vacio", JOptionPane.INFORMATION_MESSAGE);
            } else {
                Especialidad especilidad = especialidadDAO.findById(labelModificarClaveEspecialidad.getText(), true);
                especilidad.setNombre(textModificarNombreEspecialidad.getText());
                especialidadDAO.makePersistent(especilidad);
                cleanFields(MODIFICAR);
                JOptionPane.showMessageDialog(null, "La especilidad se ha modificado correctamente", "Modificación completada", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "La especialidad que indico no se ha encontrado\n" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error inesperado.\nDetalle del error:" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(JPanelAdministrarEspecialidad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void delete() {
        try {
            Especialidad especialidad = especialidadDAO.findById(labelEliminarClaveEspecialidad.getText(), true);
            especialidadDAO.makeTransient(especialidad);
            cleanFields(ELIMINAR);
            JOptionPane.showMessageDialog(null, "La especialidad se ha eliminado correctamente", "Eliminación completada", JOptionPane.INFORMATION_MESSAGE);
            updateTable();
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "La especialidad que indico no se ha encontrado\n" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error inesperado.\nDetalle del error:" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(JPanelAdministrarEspecialidad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    @Override
    public void find(int field) {
        if (field == (BUSCAR | ELIMINAR)) {
            if (!isEmptyFields((BUSCAR | ELIMINAR))) {
                try {
                    Especialidad especialidad = especialidadDAO.findById(textBuscarEliminarEspecialidad.getText(), true);
                    labelEliminarClaveEspecialidad.setText(String.valueOf(especialidad.getClave()));
                    labelEliminarNombreEspecialidad.setText(especialidad.getNombre());
                    cleanFields(BUSCAR | ELIMINAR);
                    botonEliminarEspecialidad.setEnabled(true);
                } catch (EntryNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "La especialidad que indico no se ha encontrado\n" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Error inesperado.\nDetalle del error:" + ex.getMessage(), "Especialidad no encontrada", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(JPanelAdministrarEspecialidad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe ingresar una clave para realizar la busqueda", "No se puede realizar la busqueda", JOptionPane.WARNING_MESSAGE);
            }
        } else if (field == (BUSCAR | MODIFICAR)) {
            if (!isEmptyFields((BUSCAR | MODIFICAR))) {
                try {
                    Especialidad especialidad = especialidadDAO.findById(textBuscarModificarEspecialidad.getText(), true);
                    labelModificarClaveEspecialidad.setText(especialidad.getClave());
                    textModificarNombreEspecialidad.setText(especialidad.getNombre());
                    cleanFields(BUSCAR | MODIFICAR);
                    botonModificarEspecialidad.setEnabled(true);
                } catch (EntryNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "La especialidad que indico no se ha encontrado\n" + ex.getMessage(), "Especialidad no encontrado", JOptionPane.ERROR_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Error inesperado.\nDetalle del error:" + ex.getMessage(), "Especialidad no encontrado", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(JPanelAdministrarEspecialidad.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe ingresar una clave para realizar la busqueda", "No se puede realizar la busqueda", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    @Override
    public void cancelFind(int field) {
    }

    @Override
    public boolean isEmptyFields(int field) {
        switch (field) {
            case REGISTRAR:
                return textRegistrarNombreEspecialidad.getText().isEmpty() || textRegistrarClaveEspecialidad.getText().isEmpty();
            case MODIFICAR:
                return textModificarNombreEspecialidad.getText().isEmpty();
            case BUSCAR | MODIFICAR:
                return textBuscarModificarEspecialidad.getText().isEmpty();
            case BUSCAR | ELIMINAR:
                return textBuscarEliminarEspecialidad.getText().isEmpty();
            case ELIMINAR:
                return textBuscarEliminarEspecialidad.getText().isEmpty();
            default:
                return false;
        }
    }

    @Override
    public void cleanFields(int fields) {
        switch (fields) {
            case REGISTRAR:
                textRegistrarClaveEspecialidad.setText("");
                textRegistrarNombreEspecialidad.setText("");
                break;
            case MODIFICAR:
                labelModificarClaveEspecialidad.setText("-----------");
                textModificarNombreEspecialidad.setText("");
                textModificarNombreEspecialidad.setEditable(false);
                botonModificarEspecialidad.setEnabled(false);
                break;
            case ELIMINAR:
                labelEliminarClaveEspecialidad.setText("-----------");
                labelEliminarNombreEspecialidad.setText("-----------");
                botonEliminarEspecialidad.setEnabled(false);
                break;
            case BUSCAR | MODIFICAR:
                textBuscarModificarEspecialidad.setText("");
                textModificarNombreEspecialidad.setEditable(true);
                break;
            case BUSCAR | ELIMINAR:
                textBuscarEliminarEspecialidad.setText("");
                break;
            default:
                throw new IllegalArgumentException("Valor tab invalido");
        }
    }

    @Override
    public void updateTable() {
        TableModelEspecialidad tme = (TableModelEspecialidad) jTable1.getModel();
        tme.updateModel(especialidadDAO.findAll());
    }

    private static final long serialVersionUID = -2748480462897999964L;
    private EspecialidadDAO especialidadDAO;
    private FormListener formListener;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscarEliminarEspecialidad;
    private javax.swing.JButton botonBuscarModificarEspecialidad;
    private javax.swing.JButton botonEliminarEspecialidad;
    private javax.swing.JButton botonModificarEspecialidad;
    private javax.swing.JButton botonRegistrarEspecialidad;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanelCatalogo;
    private javax.swing.JPanel jPanelEliminar;
    private javax.swing.JPanel jPanelModificar;
    private javax.swing.JPanel jPanelRegistrar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JLabel labelEliminarClaveEspecialidad;
    private javax.swing.JLabel labelEliminarNombreEspecialidad;
    private javax.swing.JLabel labelModificarClaveEspecialidad;
    private javax.swing.JTextField textBuscarEliminarEspecialidad;
    private javax.swing.JTextField textBuscarModificarEspecialidad;
    private javax.swing.JTextField textModificarNombreEspecialidad;
    private javax.swing.JTextField textRegistrarClaveEspecialidad;
    private javax.swing.JTextField textRegistrarNombreEspecialidad;
    // End of variables declaration//GEN-END:variables
}