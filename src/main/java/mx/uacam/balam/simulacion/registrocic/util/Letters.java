/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

/**
 * This class help to format a string a title style.
 * 
 * @author Freddy Barrera
 */
public class Letters {

    private String text = "";

    public Letters(String text) {
        this.text = transformText(text);
    }

    @Override
    public String toString() {
        return text;
    }

    /**
     * This method transform whole letter in the string to lower case except the first, as title
     * style and remove not letters.
     * 
     * @param text a string to transform.
     * @return a new {@code String} as title style.
     */
    private String transformText(String text) {
        StringBuilder sb = new StringBuilder();
        text = text.toLowerCase();
        for (Character c : text.toCharArray()) {
            if (Character.isLetter(c)) {
                sb.append(c);
            }
        }
        if (sb.length() > 0) {
            Character firstLetter = Character.toUpperCase(sb.charAt(0));
            sb.replace(0, 1, firstLetter.toString());
            return sb.toString();
        } else {
            return "";
        }
    }
}