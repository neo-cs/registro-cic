/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.dao;

/**
 *
 * @author Freddy Barrera
 */
public abstract class DAOFactory {

    /**
     * Creates a standalone DAOFactory that returns unmanaged DAO
     * beans for use in any environment Hibernate has been configured
     * for. Uses HibernateUtil/SessionFactory and Hibernate context
     * propagation (CurrentSessionContext), thread-bound or transaction-bound,
     * and transaction scoped.
     */
    public static final Class<HibernateDAOFactory> HIBERNATE = HibernateDAOFactory.class;

    /**
     * Factory method for instantiation of concrete factories.
     * @param factory
     * @return
     */
    public static DAOFactory getInstance(Class<? extends DAOFactory> factory) {
        try {
            return (DAOFactory) factory.newInstance();
        } catch (IllegalAccessException | InstantiationException ex) {
            throw new RuntimeException("Couldn't create DAOFactory: " + factory);
        }
    }

    // DAO interfaces
    public abstract AlumnoDAO getAlumnoDAO();
    public abstract ComputadoraDAO getComputadoraDAO();
    public abstract EspecialidadDAO getEspecialidadDAO();
    public abstract EstadoPcDAO getEstadoPcDAO();
    public abstract LocalizacionDAO getLocalizacionDAO();
    public abstract MaestroDAO getMaestroDAO();
    public abstract PrestamoPcDAO getPrestamoPcDAO();
    public abstract SalaDAO getSalaDAO();
    public abstract SoftwareDAO getSoftwareDAO();
}
