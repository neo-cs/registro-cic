/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.data.entities;
// Generated 29-jun-2010 12:20:47 by Hibernate Tools 3.2.1.GA

import java.util.HashSet;
import java.util.Set;

/**
 * Software generated by hbm2java
 */
public class Software implements java.io.Serializable {

    private static final long serialVersionUID = 7337073710684065832L;
    private Short idSoftware;
    private String nombre;
    private Set<PrestamoSala> prestamoSalas = new HashSet<>();
    private Set<PrestamoPc> prestamoPcs = new HashSet<>();

    public Software() {
    }

    public Software(String nombre) {
        this.nombre = nombre;
    }

    public Software(Short idSoftware, String nombre) {
        this.idSoftware = idSoftware;
        this.nombre = nombre;
    }

    public Software(String nombre, Set<PrestamoSala> prestamoSalas, Set<PrestamoPc> prestamoPcs) {
        this.nombre = nombre;
        this.prestamoSalas = prestamoSalas;
        this.prestamoPcs = prestamoPcs;
    }

    public Short getIdSoftware() {
        return this.idSoftware;
    }

    public void setIdSoftware(Short idSoftware) {
        this.idSoftware = idSoftware;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<PrestamoSala> getPrestamoSalas() {
        return this.prestamoSalas;
    }

    public void setPrestamoSalas(Set<PrestamoSala> prestamoSalas) {
        this.prestamoSalas = prestamoSalas;
    }

    public Set<PrestamoPc> getPrestamoPcs() {
        return this.prestamoPcs;
    }

    public void setPrestamoPcs(Set<PrestamoPc> prestamoPcs) {
        this.prestamoPcs = prestamoPcs;
    }

    @Override
    public String toString() {
        return nombre;
    }
}