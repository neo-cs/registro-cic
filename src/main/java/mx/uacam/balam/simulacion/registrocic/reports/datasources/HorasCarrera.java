/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.reports.datasources;

import java.util.ArrayList;
import java.util.Calendar;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Freddy Barrera
 */
public class HorasCarrera extends JRAbstractBeanDataSourceProvider {

    public HorasCarrera() {
        super(TablaHorasCarrera.class);
    }

    @Override
    public JRDataSource create(JasperReport jr) throws JRException {
        ArrayList<TablaHorasCarrera> collection = new ArrayList<>();
        collection.add(new TablaHorasCarrera("isc", Calendar.getInstance().getTime(),Calendar.getInstance().getTime()));
        return new JRBeanCollectionDataSource(collection);
    }

    @Override
    public void dispose(JRDataSource jrds) throws JRException {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
