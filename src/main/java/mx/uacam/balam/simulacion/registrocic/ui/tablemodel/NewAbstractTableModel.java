/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui.tablemodel;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @param <T>
 * @author Freddy Barrera
 */
public abstract class NewAbstractTableModel<T> extends AbstractTableModel {

    private static final long serialVersionUID = 7169357985587606907L;
    private static final Logger LOGGER = LogManager.getLogger();
    private Class<T> t;
    protected List<T> data;
    private List<String> columnsNames;
    private List<String> propertiesNames;
    private List<Class<?>> propertiesTypes;

    public NewAbstractTableModel(T[] data) {
        this(Arrays.asList(data));
    }

    public NewAbstractTableModel(List<T> data) {
        this.data = data;
        t = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        columnsNames = new ArrayList<>();
        propertiesNames = new ArrayList<>();
        propertiesTypes = new ArrayList<>();
        setMetaData();
    }

    private void setMetaData() {
        try {
            BeanInfo info = Introspector.getBeanInfo(t, Introspector.IGNORE_IMMEDIATE_BEANINFO);
            for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
                if ((pd.getPropertyType() != Class.class) && (pd.getPropertyType() != Set.class)) {
                    String propertyName = firstCharToUpperCase(pd.getName());
                    propertiesNames.add(propertyName);
                    String columnName = prepareTitle(pd.getName());
                    columnsNames.add(columnName);
                    Class<?> propertyType = pd.getPropertyType();
                    propertiesTypes.add(propertyType);
                }
            }
        } catch (IntrospectionException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return propertiesNames.size();
    }

    @Override
    public String getColumnName(int column) {
        return  columnsNames.get(column);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex < 0 || rowIndex > getRowCount()) {
            throw new IllegalArgumentException();
        }

        if (columnIndex < 0 || columnIndex > getColumnCount()) {
            throw new IllegalArgumentException();
        }

        String methodName = "set" + propertiesNames.get(columnIndex);

        try {
            Method method = data.get(rowIndex).getClass().getMethod(methodName, aValue.getClass());
            method.invoke(data.get(rowIndex), aValue);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object invoke = null;
        try {
            if (rowIndex < 0 || rowIndex > getRowCount()) {
                throw new IllegalArgumentException("rowIndex: " + rowIndex);
            } else if (columnIndex < 0 || columnIndex > getColumnCount()) {
                throw new IllegalArgumentException("columnIndex: " + columnIndex);
            }

            String methodName = "get" + propertiesNames.get(columnIndex);

            Method method = data.get(rowIndex).getClass().getMethod(methodName);
            invoke = method.invoke(data.get(rowIndex));
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return invoke;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return propertiesTypes.get(columnIndex);
    }

    public void updateModel(T[] data) {
        updateModel(Arrays.asList(data));
    }

    public void updateModel(List<T> data) {
        this.data = data;
        fireTableDataChanged();
    }

    private String firstCharToUpperCase(String aString) {
        char c = Character.toUpperCase(aString.charAt(0));
        return c + aString.substring(1);
    }

    private String prepareTitle(String title) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < title.length(); i++) {
            if (i == 0) {
                sb.append(Character.toUpperCase(title.charAt(i)));
            } else if (Character.isUpperCase(title.charAt(i))) {
                sb.append(' ');
                sb.append(Character.toLowerCase(title.charAt(i)));
            } else {
                sb.append(title.charAt(i));
            }
        }
        return sb.toString();
    }

}
