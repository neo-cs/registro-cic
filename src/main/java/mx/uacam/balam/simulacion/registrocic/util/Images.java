/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.jdesktop.swingx.graphics.GraphicsUtilities;

/**
 *
 * @author Freddy Barrera
 */
public enum Images {

    IMAGEN_USUARIO("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/usuario.png"),
    IMAGEN_ALUMNO("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/alumno.png"),
    IMAGEN_ACTUALIZAR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/actualizar.png"),
    IMAGEN_SALA("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/sofa.png"),
    IMAGEN_SOFTWARE("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/software.png"),
    IMAGEN_COMPUTADORAS("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/computadora.png"),
    IMAGEN_REPORTE("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/reporte.png"),
    IMAGEN_REPORTE_VER("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/reporte_ver.png"),
    IMAGEN_REPORTE_GENERAR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/reporte_generar.png"),
    IMAGEN_IMPRESORA("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/impresora.png"),
    IMAGEN_MAESTRO("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/maestro.png"),
    IMAGEN_ELIMINAR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/eliminar.png"),
    IMAGEN_MODIFICAR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/modificar.png"),
    IMAGEN_EDITAR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/editar.png"),
    IMAGEN_CATALOGO("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/catalogo.png"),
    IMAGEN_COMPUTADORA_DISPONIBLE("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/computadora_disponobles.png"),
    IMAGEN_COMPUTADORA_USO("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/computadora_en_uso.png"),
    IMAGEN_COMPUTADORA_INSERVIBLE("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/computadora_inservible.png"),
    IMAGEN_COMPUTADORA_REPARACION("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/computadora_reparacion.png"),
    IMAGEN_ADMINISTRADOR("/mx/uacam/balam/simulacion/registrocic/resources/imagenes/administrador.png");

    private final String nombreImagen;

    private Images(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public BufferedImage getImage() {
        try {
            InputStream openStream = Images.class.getClass().getResource(nombreImagen).openStream();
            return ImageIO.read(openStream);
        } catch (IOException e) {
            return null;
        }
    }

    public Icon getIcon() {
        return new ImageIcon(getImage());
    }

    public BufferedImage getImage(int width, int height) {
        return GraphicsUtilities.createThumbnail(getImage(), width, height);
    }

    public Icon getIcon(int width, int height) {
        return new ImageIcon(getImage(width, height));
    }
}
