/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.uacam.balam.simulacion.registrocic.ui;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.uacam.balam.simulacion.registrocic.data.dao.EstadoPcDAO;
import mx.uacam.balam.simulacion.registrocic.data.entities.EstadoPc;
import mx.uacam.balam.simulacion.registrocic.data.exception.DuplicateEntryException;
import mx.uacam.balam.simulacion.registrocic.data.exception.EntryNotFoundException;
import mx.uacam.balam.simulacion.registrocic.ui.tablemodel.TableModelEstadoPc;
import mx.uacam.balam.simulacion.registrocic.util.BasicOperations;
import mx.uacam.balam.simulacion.registrocic.util.FormListener;
import mx.uacam.balam.simulacion.registrocic.util.Images;
import mx.uacam.balam.simulacion.registrocic.util.OperationConstants;

/**
 *
 * @author six
 */
public class JPanelAdministrarEstadoPc extends javax.swing.JPanel
        implements OperationConstants, BasicOperations {

    /** Creates new form JPanelAdministrarLocalizaciones */
    public JPanelAdministrarEstadoPc() {
        estadoPcDAO = FACTORY.getEstadoPcDAO();
        formListener = new FormListener(this);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXHeader1 = new org.jdesktop.swingx.JXHeader();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textRegistrarDescripcionEstadoPc = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textRegistrarNombreEstadoPc = new javax.swing.JTextField();
        botonRegistrarEstadoPc = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        textBuscarModificarEstadoPc = new javax.swing.JTextField();
        botonBuscarModificarEstadoPc = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelModificarClaveEstadoPc = new javax.swing.JLabel();
        textModificarNombreEstadoPc = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        textModificarDescripcionEstadoPc = new javax.swing.JTextArea();
        botonModificarEstadoPc = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        botonBucarEliminarEstadoPc = new javax.swing.JButton();
        textBuscarEliminarEstadoPc = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        labelEliminarClaveEstadoPc = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelEliminarNombreEstadoPc = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        textEliminarDescripcionEstadoPc = new javax.swing.JTextArea();
        botonEliminarEstadoPc = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar(bundle.getString("toolBar.updateTable"));
        jButton1 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jXHeader1.setMinimumSize(new java.awt.Dimension(46, 90));
        jXHeader1.setPreferredSize(new java.awt.Dimension(51, 90));
        jXHeader1.setIcon(Images.IMAGEN_COMPUTADORAS.getIcon(64, 64));
        jXHeader1.setTitleFont(new java.awt.Font("Tahoma", 1, 18));
        jXHeader1.setTitle("Administrar estado PC");
        jXHeader1.setDescription("Esta opción le permite configurar los posibles estados que tendran las computadoras.");
        add(jXHeader1, java.awt.BorderLayout.PAGE_START);

        textRegistrarDescripcionEstadoPc.setColumns(20);
        textRegistrarDescripcionEstadoPc.setRows(5);
        jScrollPane1.setViewportView(textRegistrarDescripcionEstadoPc);

        jLabel2.setText(bundle.getString("label.estadoPc.name")); // NOI18N

        jLabel3.setText(bundle.getString("label.estadoPc.description")); // NOI18N

        botonRegistrarEstadoPc.setText(bundle.getString("button.add")); // NOI18N
        botonRegistrarEstadoPc.setActionCommand(COMMAND_ADD);
        botonRegistrarEstadoPc.addActionListener(formListener);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textRegistrarNombreEstadoPc, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)))
                    .addComponent(botonRegistrarEstadoPc))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textRegistrarNombreEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonRegistrarEstadoPc)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("tab.add"), Images.IMAGEN_EDITAR.getIcon(24, 24), jPanel1); // NOI18N

        textBuscarModificarEstadoPc.setToolTipText(bundle.getString("toolTip.estadoPc.find")); // NOI18N

        botonBuscarModificarEstadoPc.setText(bundle.getString("button.find")); // NOI18N
        botonBuscarModificarEstadoPc.setActionCommand(COMMAND_FIND_EDIT);
        botonBuscarModificarEstadoPc.addActionListener(formListener);

        jLabel1.setText(bundle.getString("label.find")); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarModificarEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBuscarModificarEstadoPc)
                .addContainerGap(121, Short.MAX_VALUE))
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textBuscarModificarEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarModificarEstadoPc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel4.setText(bundle.getString("label.estadoPc.id")); // NOI18N

        jLabel5.setText(bundle.getString("label.estadoPc.name")); // NOI18N

        jLabel6.setText(bundle.getString("label.estadoPc.description")); // NOI18N

        labelModificarClaveEstadoPc.setText(bundle.getString("label.line")); // NOI18N

        textModificarDescripcionEstadoPc.setColumns(20);
        textModificarDescripcionEstadoPc.setRows(5);
        jScrollPane2.setViewportView(textModificarDescripcionEstadoPc);

        botonModificarEstadoPc.setText(bundle.getString("button.edit")); // NOI18N
        botonModificarEstadoPc.setActionCommand(COMMAND_EDIT);
        botonModificarEstadoPc.setEnabled(false);
        botonModificarEstadoPc.addActionListener(formListener);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelModificarClaveEstadoPc, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(textModificarNombreEstadoPc, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(140, 140, 140))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(botonModificarEstadoPc)
                .addContainerGap(310, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelModificarClaveEstadoPc)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textModificarNombreEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonModificarEstadoPc)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("button.edit"), Images.IMAGEN_MODIFICAR.getIcon(24, 24), jPanel2); // NOI18N

        botonBucarEliminarEstadoPc.setText(bundle.getString("button.find")); // NOI18N
        botonBucarEliminarEstadoPc.setActionCommand(COMMAND_FIND_DELETE);
        botonBucarEliminarEstadoPc.addActionListener(formListener);

        textBuscarEliminarEstadoPc.setToolTipText(bundle.getString("toolTip.estadoPc.find")); // NOI18N

        jLabel8.setText(bundle.getString("label.estadoPc.name")); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textBuscarEliminarEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonBucarEliminarEstadoPc)
                .addContainerGap(114, Short.MAX_VALUE))
            .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(textBuscarEliminarEstadoPc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBucarEliminarEstadoPc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel7.setText(bundle.getString("label.estadoPc.id")); // NOI18N

        labelEliminarClaveEstadoPc.setText(bundle.getString("label.line")); // NOI18N

        jLabel9.setText(bundle.getString("label.estadoPc.name")); // NOI18N

        labelEliminarNombreEstadoPc.setText(bundle.getString("label.line")); // NOI18N

        jLabel11.setText(bundle.getString("label.estadoPc.description")); // NOI18N

        textEliminarDescripcionEstadoPc.setColumns(20);
        textEliminarDescripcionEstadoPc.setEditable(false);
        textEliminarDescripcionEstadoPc.setRows(5);
        textEliminarDescripcionEstadoPc.setEnabled(false);
        jScrollPane3.setViewportView(textEliminarDescripcionEstadoPc);

        botonEliminarEstadoPc.setText(bundle.getString("button.delete")); // NOI18N
        botonEliminarEstadoPc.setActionCommand(COMMAND_DELETE);
        botonEliminarEstadoPc.setEnabled(false);
        botonEliminarEstadoPc.addActionListener(formListener);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane3)
                            .addComponent(labelEliminarNombreEstadoPc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelEliminarClaveEstadoPc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(botonEliminarEstadoPc))
                .addContainerGap(151, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEliminarClaveEstadoPc)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEliminarNombreEstadoPc)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEliminarEstadoPc)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab(bundle.getString("tab.delete"), Images.IMAGEN_ELIMINAR.getIcon(24, 24), jPanel3); // NOI18N

        jPanel8.setLayout(new java.awt.BorderLayout());

        jToolBar1.setRollover(true);

        jButton1.setIcon(Images.IMAGEN_ACTUALIZAR.getIcon(20, 20));
        jButton1.setText(bundle.getString("button.updateTable")); // NOI18N
        jButton1.setToolTipText(bundle.getString("toolTip.updateTable")); // NOI18N
        jButton1.setActionCommand(COMMAND_UPDATE_TABLE);
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(formListener);
        jToolBar1.add(jButton1);

        jPanel8.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jTable1.setModel(new TableModelEstadoPc(estadoPcDAO.findAll()));
        jScrollPane4.setViewportView(jTable1);

        jPanel8.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab(bundle.getString("tab.catalog"), Images.IMAGEN_CATALOGO.getIcon(24, 24), jPanel8); // NOI18N

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void add() {
        if (isEmptyFields(REGISTRAR)) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.emptyRequestFields.message"), //NOI18N
                    bundle.getString("dialog.emptyRequestFields.title"), //NOI18N
                    JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                EstadoPc estadoPC = new EstadoPc();
                estadoPC.setNombre(textRegistrarNombreEstadoPc.getText());
                estadoPC.setDescripcion(textRegistrarDescripcionEstadoPc.getText());
                estadoPcDAO.makePersistent(estadoPC);
                JOptionPane.showMessageDialog(null, bundle.getString(
                        "dialog.saveCorrectly.message"),//NOI18N
                        bundle.getString("dialog.saveCorrectly.title"),//NOI18N
                        JOptionPane.INFORMATION_MESSAGE);
                cleanFields(REGISTRAR);
                updateTable();
            } catch (DuplicateEntryException ex) {
                JOptionPane.showMessageDialog(null, bundle.getString(
                        "dialog.duplicateEntry.message"), bundle.getString( //NOI18N
                        "dialog.duplicateEntry.title"), //NOI18N
                        JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, bundle.getString(
                        "dialog.unexpectedError.message"), bundle.getString( //NOI18N
                        "dialog.unexpectedError.title"), //NOI18N
                        JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(JPanelAdministrarEstadoPc.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    @Override
    public void edit() {
        try {
            Short id = Short.valueOf(labelModificarClaveEstadoPc.getText());
            EstadoPc estadoPc = estadoPcDAO.findById(id, true);
            estadoPc.setNombre(textModificarNombreEstadoPc.getText());
            estadoPc.setDescripcion(textModificarDescripcionEstadoPc.getText());
            estadoPcDAO.makePersistent(estadoPc);
            cleanFields(MODIFICAR);
            JOptionPane.showMessageDialog(null, bundle.getString(
                        "dialog.editCorrectly.message"),//NOI18N
                        bundle.getString("dialog.editCorrectly.title"),//NOI18N
                        JOptionPane.INFORMATION_MESSAGE);
        } catch (DuplicateEntryException ex) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.duplicateEntry.message"), bundle.getString(//NOI18N
                    "dialog.duplicateEntry.title"), JOptionPane.ERROR_MESSAGE);//NOI18N
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.entityNotFound.message"), bundle.getString(//NOI18N
                    "dialog.entityNotFound.title"), JOptionPane.ERROR_MESSAGE);//NOI18N
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.unexpectedError.message"), bundle.getString( //NOI18N
                    "dialog.unexpectedError.title"), JOptionPane.ERROR_MESSAGE); //NOI18N
            Logger.getLogger(JPanelAdministrarEstadoPc.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void delete() {
        try {
            Short id = Short.valueOf(labelEliminarClaveEstadoPc.getText());
            EstadoPc estadoPc = estadoPcDAO.findById(id, true);
            estadoPcDAO.makeTransient(estadoPc);
            cleanFields(ELIMINAR);
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.deleteCorrectly.message"), bundle.getString( //NOI18N
                    "dialog.deleteCorrectly.title"), JOptionPane.INFORMATION_MESSAGE); //NOI18N
        } catch (EntryNotFoundException ex) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.entityNotFound.message"), bundle.getString( //NOI18N
                    "dialog.entityNotFound.title"), JOptionPane.ERROR_MESSAGE);//NOI18N
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, bundle.getString(
                    "dialog.unexpectedError.message"), bundle.getString(//NOI18N
                    "dialog.unexpectedError.title"), JOptionPane.ERROR_MESSAGE);//NOI18N
            Logger.getLogger(JPanelAdministrarEstadoPc.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void find(int field) {
        switch (field) {
            case BUSCAR | MODIFICAR:
                if (isEmptyFields(field)) {
                    JOptionPane.showMessageDialog(null, bundle.getString(
                            "dialog.emptyField.message"), bundle.getString( //NOI18N
                            "dialog.emptyField.title"), //NOI18N
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    EstadoPc example = new EstadoPc();
                    String[] excludeProperty = {"clave", "descripcion", "computadoras"}; //NOI18N
                    example.setNombre(textBuscarModificarEstadoPc.getText());
                    List<EstadoPc> findByExample = estadoPcDAO.findByExample(example, excludeProperty);
                    if (findByExample.size() > 0) {
                        example = findByExample.get(0);
                        labelModificarClaveEstadoPc.setText(example.getClave().toString());
                        textModificarNombreEstadoPc.setText(example.getNombre());
                        textModificarDescripcionEstadoPc.setText(example.getDescripcion());
                        cleanFields(BUSCAR | MODIFICAR);
                    } else {
                        JOptionPane.showMessageDialog(null, bundle.getString(
                                "dialog.entityNotFound.message"), //NOI18N
                                bundle.getString("dialog.entityNotFound.title"),//NOI18N
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            case BUSCAR | ELIMINAR:
                if (isEmptyFields(field)) {
                    JOptionPane.showMessageDialog(null,
                            bundle.getString("dialog.emptyField.message"),//NOI18N
                            bundle.getString("dialog.emptyField.title"), //NOI18N
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    EstadoPc example = new EstadoPc();
                    String[] excludeProperty = {"clave", "descripcion", "computadoras"}; //NOI18N
                    example.setNombre(textBuscarEliminarEstadoPc.getText());
                    List<EstadoPc> findByExample = estadoPcDAO.findByExample(example, excludeProperty);
                    if (findByExample.size() > 0) {
                        example = findByExample.get(0);
                        labelEliminarClaveEstadoPc.setText(example.getClave().toString());
                        labelEliminarNombreEstadoPc.setText(example.getNombre());
                        textEliminarDescripcionEstadoPc.setText(example.getDescripcion());
                        cleanFields(BUSCAR | ELIMINAR);
                    } else {
                        JOptionPane.showMessageDialog(null,
                                bundle.getString("dialog.entityNotFound.message"),//NOI18N
                                bundle.getString("dialog.entityNotFound.title"),//NOI18N
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
        }
    }

    @Override
    public void cancelFind(int field) {
    }

    @Override
    public boolean isEmptyFields(int field) {
        switch (field) {
            case REGISTRAR:
                return textRegistrarNombreEstadoPc.getText().isEmpty();
            case BUSCAR | MODIFICAR:
                return textBuscarModificarEstadoPc.getText().isEmpty();
            case BUSCAR | ELIMINAR:
                return textBuscarEliminarEstadoPc.getText().isEmpty();
            default:
                throw new IllegalArgumentException("input :" + field); //NOI18N
        }
    }

    @Override
    public void cleanFields(int fields) {
        switch (fields) {
            case REGISTRAR:
                textRegistrarNombreEstadoPc.setText("");
                textRegistrarDescripcionEstadoPc.setText("");
                break;
            case MODIFICAR:
                labelModificarClaveEstadoPc.setText(bundle.getString(
                        "label.line")); //NOI18N
                textModificarNombreEstadoPc.setText("");
                textModificarDescripcionEstadoPc.setText("");
                botonModificarEstadoPc.setEnabled(false);
                break;
            case BUSCAR | MODIFICAR:
                textBuscarModificarEstadoPc.setText("");
                botonModificarEstadoPc.setEnabled(true);
                break;
            case BUSCAR | ELIMINAR:
                textBuscarEliminarEstadoPc.setText("");
                botonEliminarEstadoPc.setEnabled(true);
                break;
            case ELIMINAR:
                labelEliminarClaveEstadoPc.setText(bundle.getString("label.line"));//NOI18N
                labelEliminarNombreEstadoPc.setText(bundle.getString("label.line"));//NOI18N
                textEliminarDescripcionEstadoPc.setText("");
                botonEliminarEstadoPc.setEnabled(false);
                break;
        }
    }

    @Override
    public void updateTable() {
        TableModelEstadoPc tmep = (TableModelEstadoPc) jTable1.getModel();
        tmep.updateModel(estadoPcDAO.findAll());
    }

    private static final long serialVersionUID = 5957440263844058252L;
    private final EstadoPcDAO estadoPcDAO;
    private final FormListener formListener;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBucarEliminarEstadoPc;
    private javax.swing.JButton botonBuscarModificarEstadoPc;
    private javax.swing.JButton botonEliminarEstadoPc;
    private javax.swing.JButton botonModificarEstadoPc;
    private javax.swing.JButton botonRegistrarEstadoPc;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.swingx.JXHeader jXHeader1;
    private javax.swing.JLabel labelEliminarClaveEstadoPc;
    private javax.swing.JLabel labelEliminarNombreEstadoPc;
    private javax.swing.JLabel labelModificarClaveEstadoPc;
    private javax.swing.JTextField textBuscarEliminarEstadoPc;
    private javax.swing.JTextField textBuscarModificarEstadoPc;
    private javax.swing.JTextArea textEliminarDescripcionEstadoPc;
    private javax.swing.JTextArea textModificarDescripcionEstadoPc;
    private javax.swing.JTextField textModificarNombreEstadoPc;
    private javax.swing.JTextArea textRegistrarDescripcionEstadoPc;
    private javax.swing.JTextField textRegistrarNombreEstadoPc;
    // End of variables declaration//GEN-END:variables
}